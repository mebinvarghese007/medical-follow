angular.module('app.routes', [])

        .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

            $ionicConfigProvider.backButton.previousTitleText(false);
            $ionicConfigProvider.backButton.text("");

            $stateProvider

                    .state('login', {
                        url: '/login',
                        templateUrl: 'templates/login.html',
                        controller: 'loginCtrl'
                    })

                    .state('forgot_password', {
                        url: '/forgot_password',
                        templateUrl: 'templates/forgot-password.html',
                        controller: 'forgotPasswordCntrl'
                    })

                    .state('tab', {
                        url: '/tab',
                        templateUrl: 'templates/tab.html',
                        abstract: true
                    })

                    .state('tab.home', {
                        cache: false,
                        url: '/home',
                        views: {
                            'tab-home': {
                                templateUrl: 'templates/home.html',
                                controller: 'homeCtrl'
                            }
                        }
                    })

                    .state('tab.patient_search', {
                        cache: false,
                        url: '/patient_search',
                        views: {
                            'tab-patients': {
                                templateUrl: 'templates/patient_search.html',
                                controller: 'patientSearchCtrl'
                            }
                        }
                    })

                    .state('tab.patients', {
                        cache: false,
                        url: '/patients',
                        params: {
                            patients: null
                        },
                        views: {
                            'tab-patients': {
                                templateUrl: 'templates/patients.html',
                                controller: 'patientsCtrl'
                            }
                        }
                    })

                    .state('tab.patient', {
                        cache: false,
                        url: '/patient/:patientId',
                        views: {
                            'tab-patients': {
                                templateUrl: 'templates/patient.html',
                                controller: 'patientCtrl'
                            }
                        }
                    })

                    .state('tab.study', {
                        cache: false,
                        url: '/study/:patientId/:studyId',
                        views: {
                            'tab-patients': {
                                templateUrl: 'templates/study.html',
                                controller: 'studyCtrl'
                            }
                        }
                    })

                    .state('tab.settings', {
                        cache: false,
                        url: '/settings',
                        views: {
                            'tab-settings': {
                                templateUrl: 'templates/settings.html',
                                controller: 'settingsCtrl'
                            }
                        }
                    })

                    .state('tab_erp', {
                        url: '/tab_erp',
                        templateUrl: 'templates/erp/tab.html',
                        abstract: true
                    })

                    .state('tab_erp.home', {
                        cache: false,
                        url: '/home',
                        views: {
                            'tab_erp-home': {
                                templateUrl: 'templates/erp/home.html',
                                controller: 'homeErpCtrl'
                            }
                        }
                    })

                    .state('tab_erp.patient_search', {
                        cache: false,
                        url: '/patient_search',
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/patient_search.html',
                                controller: 'patientErpSearchCtrl'
                            }
                        }
                    })

                    .state('tab_erp.patients', {
                        cache: false,
                        url: '/patients',
                        params: {
                            patients: null
                        },
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/patients.html',
                                controller: 'patientsErpCtrl'
                            }
                        }
                    })

                    .state('tab_erp.patient_create', {
                        cache: false,
                        url: '/patient/create',
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/patient_create.html',
                                controller: 'patientCreateErpCtrl'
                            }
                        }
                    })

                    .state('tab_erp.patient', {
                        cache: false,
                        url: '/patient/:patientId',
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/patient.html',
                                controller: 'patientErpCtrl'
                            }
                        }
                    })

                    .state('tab_erp.patient_procedures', {
                        cache: false,
                        url: '/patient/procedures/:patientId',
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/form/procedures.html',
                                controller: 'proceduresErpCtrl'
                            }
                        }
                    })

                    .state('tab_erp.form_hombro_consulta', {
                        cache: false,
                        url: '/form/hombro/consulta/:patientId',
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/form/consulta_control.html',
                                controller: 'ERP.hombroConsultaCtrl'
                            }
                        }
                    })

                    .state('tab_erp.form_hombro_cirugia', {
                        cache: false,
                        url: '/form/hombro/cirugia/:patientId',
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/form/cirugia.html',
                                controller: 'ERP.hombroCirugiaCtrl'
                            }
                        }
                    })

                    .state('tab_erp.form_hombro_control', {
                        cache: false,
                        url: '/form/hombro/control/:patientId',
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/form/consulta_control.html',
                                controller: 'ERP.hombroControlCtrl'
                            }
                        }
                    })

                    .state('tab_erp.form_cadera_consulta', {
                        cache: false,
                        url: '/form/cadera/consulta/:patientId',
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/form/consulta_control.html',
                                controller: 'ERP.caderaConsultaCtrl'
                            }
                        }
                    })

                    .state('tab_erp.form_cadera_cirugia', {
                        cache: false,
                        url: '/form/cadera/cirugia/:patientId',
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/form/cirugia.html',
                                controller: 'ERP.caderaCirugiaCtrl'
                            }
                        }
                    })

                    .state('tab_erp.form_cadera_control', {
                        cache: false,
                        url: '/form/cadera/control/:patientId',
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/form/consulta_control.html',
                                controller: 'ERP.caderaControlCtrl'
                            }
                        }
                    }) 

                    .state('tab_erp.form_rodilla_consulta', {
                        cache: false,
                        url: '/form/rodilla/consulta/:patientId',
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/form/consulta_control.html',
                                controller: 'ERP.rodillaConsultaCtrl'
                            }
                        }
                    })

                    .state('tab_erp.form_rodilla_cirugia', {
                        cache: false,
                        url: '/form/rodilla/cirugia/:patientId',
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/form/cirugia.html',
                                controller: 'ERP.rodillaCirugiaCtrl'
                            }
                        }
                    })

                    .state('tab_erp.form_rodilla_control', {
                        cache: false,
                        url: '/form/rodilla/control/:patientId',
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/form/consulta_control.html',
                                controller: 'ERP.rodillaControlCtrl'
                            }
                        }
                    }) 

                    .state('tab_erp.form_columna_consulta', {
                        cache: false,
                        url: '/form/columna/consulta/:patientId',
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/form/consulta_control.html',
                                controller: 'ERP.columnaConsultaCtrl'
                            }
                        }
                    })

                    .state('tab_erp.form_columna_cirugia', {
                        cache: false,
                        url: '/form/columna/cirugia/:patientId',
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/form/cirugia.html',
                                controller: 'ERP.columnaCirugiaCtrl'
                            }
                        }
                    })

                    .state('tab_erp.form_columna_control', {
                        cache: false,
                        url: '/form/columna/control/:patientId',
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/form/consulta_control.html',
                                controller: 'ERP.columnaControlCtrl'
                            }
                        }
                    })
                    .state('tab_erp.patientedit', {
                        cache: false,
                        url: '/patientEdit/:patientId',
                        views: {
                            'tab_erp-patient_search': {
                                templateUrl: 'templates/erp/patient_edit.html',
                                controller: 'patientEditErpCtrl'
                            }
                        }
                    })
                    .state('tab_erp.settings', {
                        cache: false,
                        url: '/settings',
                        views: {
                            'tab_erp-settings': {
                                templateUrl: 'templates/erp/settings.html',
                                controller: 'settingsErpCtrl'
                            }
                        }
                    })
                    .state('tab_erp.history_details', {
                        url: '/history_details',
                        params: {
                            historyData: null,
                            type: null,
                            images : null
                        },
                        cache: false,
                        views: {
                            'tab_erp-history': {
                                templateUrl: 'templates/erp/history_details.html',
                                controller: 'historyDetailsCtrl'
                            }
                        }
                    })

                    .state('tab_erp.patient_history_search', {
                        cache: false,
                        url: '/patient_history_search',
                        views: {
                            'tab_erp-history': {
                                templateUrl: 'templates/erp/patient_search.html',
                                controller: 'patientHistoryErpSearchCtrl'
                            }
                        }
                    })
                    .state('tab_erp.history', {
                        cache: false,
                        url: '/history',
                        params: {
                            id: null
                        },
                        views: {
                            'tab_erp-history': {
                                templateUrl: 'templates/erp/history.html',
                                controller: 'historyCtrl'
                            }
                        }
                    })

                    .state('tab_erp.patients_history', {
                        cache: false,
                        url: '/patients',
                        params: {
                            patients: null
                        },
                        views: {
                            'tab_erp-history': {
                                templateUrl: 'templates/erp/patients.html',
                                controller: 'historyPatientsErpCtrl'
                            }
                        }
                    });

            $urlRouterProvider.otherwise('/login')

        })

        .config(['$httpProvider', function ($httpProvider) {
                $httpProvider.interceptors.push(['$location', '$q', function ($location, $q) {
                        return {
                            'request': function (request) {
                                return request;
                            },
                            'responseError': function (response) {
                                if (response.status === 401) {
                                    // do stuff
                                }
                                // otherwise, default behaviour
                                return $q.reject(response);
                            }
                        };
                    }]);
            }]);