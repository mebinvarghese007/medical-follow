angular.module('app.directives', [])

.directive('blankDirective', [function(){

}])
.directive('deleNumber', function(){
	 return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.$watch(attrs.ngModel, function (v) {
            	if(v!== undefined){
            		console.log(v)
            		if (v.match(/[0-9]/)) {

				 		console.log('value changed, new value is: ' + v);
				      v = v.replace(/[^0-9]/g, '');
				      attrs.ngModel.$setViewValue(v);
				 }
            	}
            });
        }
    };
})
.directive('onlyLettersInput',function onlyLettersInput() {
      return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModelCtrl) {
          function fromUser(text) {
            var transformedInput = text.replace(/[^a-zA-Z ]+$/g, '');
            //console.log(transformedInput);
            if (transformedInput !== text) {
              ngModelCtrl.$setViewValue(transformedInput);
              ngModelCtrl.$render();
            }
            return transformedInput;
          }
          ngModelCtrl.$parsers.push(fromUser);
        }
      };
    })

.filter('caprmundrscr', function() {
    return function(input) {
        input = input.replace(/[^a-zA-Z0-9]/g,' ');
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
})
.filter('tipofilter', function() {
    return function(input) {
        input = input.replace(/[^a-zA-Z0-9]/g,'-');
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});
// .directive('ignoreAccents',function ignoreAccents() {
//       return {
//         require: 'ngModel',
//         link: function(scope, element, attr, ngModelCtrl) {
//           function fromUser(text) {
//             var transformedInput = removeAccents(text);
//             console.log(transformedInput);
//             console.log(text);
//             if (transformedInput !== text) {
//               ngModelCtrl.$setViewValue(transformedInput);
//               ngModelCtrl.$render();
//             }
//             return transformedInput;
//           }
//           function removeAccents(value) {
//                 return value
//                     .replace(/á/g, 'a')            
//                     .replace(/é/g, 'e')
//                     .replace(/í/g, 'i')
//                     .replace(/ó/g, 'o')
//                     .replace(/ú/g, 'u');
//           }
//           ngModelCtrl.$parsers.push(fromUser);
//         }
//       };
//     })
// .filter('removeaccent', function() {
//     return function(input) {
//         input = removeAccents(input);
//           function removeAccents(value) {
//                 return value
//                     .replace(/á/g, 'a')            
//                     .replace(/é/g, 'e')
//                     .replace(/í/g, 'i')
//                     .replace(/ó/g, 'o')
//                     .replace(/ú/g, 'u');
//           }
//       return input;
//     }
// });
// .directive('ignoreAccents', function(){
//    return {
//         restrict: 'A',
//         link: function (scope, element, attrs) {
//             scope.$watch(attrs.ngModel, function (v) {
//               if(v!== undefined){
//                 console.log(v)
//                 if (v.match(/[0-9]/)) {

//                   console.log('value changed, new value is: ' + v);
//                     v = v.replace(/[^0-9]/g, '');
//                     attrs.ngModel.$setViewValue(v);
//                }
//               }
//             });


//                function removeAccents(value) {
//                 return value
//                     .replace(/á/g, 'a')            
//                     .replace(/é/g, 'e')
//                     .replace(/í/g, 'i')
//                     .replace(/ó/g, 'o')
//                     .replace(/ú/g, 'u');
//               }
//         }
//     };
// });
// .directive('ignoreAccents', function ignoreAccents() {
//     return{
//       require: 'ngModel',
//       link: function(scope,element,attrs,ngModelCtrl){
//         function fromUser(text) {
//             console.log(text);
//             return text;
//           }
//       }
//     }
// });

// .filter('ignoreAccents', function() {
//     return function(input) {
//        function removeAccents(value) {
//         return value
//             .replace(/á/g, 'a')            
//             .replace(/é/g, 'e')
//             .replace(/í/g, 'i')
//             .replace(/ó/g, 'o')
//             .replace(/ú/g, 'u');
//       }
//       return function(input) {               
//           if (input)
//               return true;       
//           var text = removeAccents(item.toLowerCase())
//           var search = removeAccents(input.toLowerCase());
//           return text.indexOf(search) > -1;
//       };
//     }
// });
