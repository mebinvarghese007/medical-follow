angular.module('app.controllers')

.controller('studyCtrl', function($scope, $state, $rootScope, $stateParams, $http, $sce, FlashMessage, Auth, $ionicActionSheet, ImageService, $timeout) {

    $scope.stage = {};
    $scope.sections = {};
    $scope.question = {};
    $scope.indexActive = 1;
    $scope.url = $rootScope.endPoint + 'patient/study/' + $stateParams.patientId + '/' + $stateParams.studyId + '?token=' + Auth.token();
    $scope.ui = {cancelButton: 'Cancelar'};

    if ($stateParams.patientId > 0 && $stateParams.studyId > 0) {
        $scope.showLoading();

        $http.get($scope.url).
        success(function(response){
            $scope.hideLoading();
            $scope.stage = response.stage;
            $scope.sections = response.sections;
            $scope.queueRefreshQuestions();
        }).
        error(function(error, errorCode){
            $scope.hideLoading();
            if (errorCode == 401) {
                Auth.logout();
            } else {
                FlashMessage.showError(error != null && typeof error.message != 'undefined' ? error.message : 'Ha ocurrido un error, intente nuevamente', function() {
                    $state.go('tab.patient', {patientId: $stateParams.patientId});
                });
            }
        });
    } else {
        $state.go('tab.patient_search');
    }

    $scope.addMedia = function(questionId) {
        $scope.hideSheet = $ionicActionSheet.show({
            titleText: 'Agregar Imagen',
            buttons: [
                { text: 'Tomar Foto' },
                { text: 'Escoger de la Galería' }
            ],
            cancelText: 'Cancelar',
            buttonClicked: function(index) {
                $scope.addImage(index, questionId);
            }
        });
    };

    $scope.addImage = function(type, questionId) {
        $scope.hideSheet();
        ImageService.handleMediaDialog(type, function(err, response) {
            if (! err) {
                $('#images_' + questionId).val(response);
            }
            $scope.$apply();
        });
    };

    $scope.save = function() {
        $scope.showLoading();

        $.post($scope.url, $('#frmPatientStudy').serialize(), function() {
            $scope.hideLoading();
            FlashMessage.showInformation('El estudio del paciente ha sido guardado', function() {
                $scope.indexActive = 1;
                $state.go('tab.patient', {patientId: $stateParams.patientId});
            });
        }).
        error(function() {
            $scope.hideLoading();
            FlashMessage.showError('Ha ocurrido un error, intente nuevamente');
        });
    };

    $scope.renderHtml = function(html_code) {
        return $sce.trustAsHtml(html_code);
    };

    $scope.displayIf = function(index) {
        return index == $scope.indexActive;
    };

    $scope.previousSection = function() {
        $scope.indexActive--;
    };

    $scope.nextSection = function() {
        $scope.indexActive++;
    };

    $scope.displayIfFirst = function(index) {
        return index == 1 && $scope.sections.length > 1
    };

    $scope.displayIfMiddle = function(index) {
        return index > 1 && index < $scope.sections.length;
    };

    $scope.displayIfLast = function(index) {
        return index == $scope.sections.length;
    };

    $scope.isUndefined = function (thing) {
        return (typeof thing === "undefined");
    };

    $scope.refreshQuestions = function() {
        $('[data-require-id]').each(function() {
            var questionId = $(this).data('requireId'),
                answers = ($(this).data('requireAnswers')+'').split(",");

            if (questionId > 0 && answers.length > 0) {
                var $requiredQuestion = $('[name="question['+questionId+']"]'),
                    displayField = false;

                //console.log($(this).data("name") + ' ' + answers + ' => ' + $requiredQuestion.val() + ' => ' + answers.indexOf($requiredQuestion.val()));

                // Is Normal field?
                if ($requiredQuestion.length > 0) {
                    if (answers.indexOf($requiredQuestion.val()) != -1) {
                        displayField = true;
                    }
                } else {
                    // Is Checkbox field?
                    $requiredQuestion = $('[name="question['+questionId+'][]"]');

                    if ($requiredQuestion.length > 0) {
                        $requiredQuestion.each(function() {
                            if (answers.indexOf($(this).val()) != -1 && $(this).is(":checked")) {
                                displayField = true;
                            }
                        });
                    }
                }

                if (displayField) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            }
        });
    };

    $scope.queueRefreshQuestions = function() {
        $timeout($scope.refreshQuestions, 100);
    };

    $(document).on('click', '.item-checkbox input', function() {
        $scope.queueRefreshQuestions();
    });
});
