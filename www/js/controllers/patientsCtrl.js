angular.module('app.controllers')

.controller('patientsCtrl', function($scope, $state, $rootScope, $stateParams, Auth) {
    $scope.patients = $stateParams.patients;

    if ($scope.patients == null) {
        $state.go('tab.patient_search');
    }

    $scope.getPatient = function(id) {
        $state.go('tab.patient', {patientId: id});
    };

    Auth.checkLoggedOnline();
});
