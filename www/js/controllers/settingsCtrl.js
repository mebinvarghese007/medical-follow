angular.module('app.controllers')

.controller('settingsCtrl', function($scope, $state, $rootScope, $http, $filter, FlashMessage, Auth, ionicDatePicker) {

    $scope.profile = {};
    $scope.birth_date = new Date(1980, 0, 1);

    var inputDateParams = {
        callback: function (val) {  //Mandatory
            var date = new Date(val),
                day = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()),
                month = (date.getMonth() + 1),
                month = (month < 10 ? '0' + month : month);

            $scope.profile.doctor.birth_date = date.getFullYear() + '-' + month + '-' + day;
            $scope.birth_date = new Date(val);
        },
        setLabel: 'Seleccionar',
        closeOnSelect: false,
        templateType: 'popup',
        from: new Date(1920, 1, 1), //Optional
        to: new Date(2016, 10, 30), //Optional
    };

    $scope.openDatePicker = function(){
        inputDateParams.inputDate = $scope.birth_date;
        ionicDatePicker.openDatePicker(inputDateParams);
    };

    $scope.loadProfile = function(){
        $scope.showLoading();

        $http.get($rootScope.endPoint + 'auth/profile?token=' + Auth.token()).
        success(function(response){
            $scope.hideLoading();
            $scope.profile = response;

            if (typeof response.doctor.birth_date_parts != 'undefined') {
                var birth_date = response.doctor.birth_date_parts;
                $scope.birth_date = new Date(birth_date[0], (birth_date[1] * 1) - 1, birth_date[2]);//new Date(response.doctor.birth_date);
            } else {
                $scope.profile.doctor.birth_date = 'Ninguna';
            }
        }).
        error(function(error, errorCode){
            $scope.hideLoading();
            if (errorCode == 401) {
                Auth.logout();
            } else {
                FlashMessage.showError('Ha ocurrido un error, intente nuevamente');
            }
        });
    };

    $scope.updateProfile = function() {
        $scope.showLoading();

        $http.post($rootScope.endPoint + 'auth/profile?token=' + Auth.token(), $scope.profile.doctor).
        success(function(response){
            $scope.hideLoading();
            if (response.success) {
                FlashMessage.showInformation('La información de perfil ha sido actualizada');
            } else {
                FlashMessage.showError('Ha ocurrido un error, por favor intente nuevamente');
            }
        }).
        error(function(error, errorCode){
            $scope.hideLoading();
            if (errorCode == 401) {
                Auth.logout();
            } else {
                FlashMessage.showError('Ha ocurrido un error, por favor intente nuevamente');
            }
        });
    };

    $scope.loadProfile();

    $scope.logout = function() {
        Auth.logout();
    };

});
