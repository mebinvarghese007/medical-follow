angular.module('app.controllers')

.controller('patientCtrl', function($scope, $state, $rootScope, $stateParams, $http, FlashMessage, Auth) {

    $scope.token = Auth.token();

    if ($stateParams.patientId > 0) {
        $scope.showLoading();

        $http.get($rootScope.endPoint + 'patient/' + $stateParams.patientId + '?token=' + Auth.token()).
        success(function(response){
            $scope.hideLoading();
            $scope.patient = response;
        }).
        error(function(error, errorCode){
            $scope.hideLoading();
            if (errorCode == 401) {
                Auth.logout();
            } else {
                FlashMessage.showError('Ha ocurrido un error, intente nuevamente');
                $state.go('tab.patient_search');
            }
        });
    } else {
        $state.go('tab.patient_search');
    }

    $scope.openStudy = function(studyId) {
        $state.go('tab.study', {patientId: $stateParams.patientId, studyId:studyId});
    };

});
