angular.module('app.controllers')

.controller('homeCtrl', function($scope, $state, $rootScope, $http, FlashMessage, Auth) {

    $scope.profile = {};

    $scope.loadProfile = function(){
        $scope.showLoading();

        $http.get($rootScope.endPoint + 'auth/profile?token=' + Auth.token()).
        success(function(response){
            $scope.hideLoading();
            $scope.profile = response;
        }).
        error(function(error, errorCode){
            $scope.hideLoading();
            if (errorCode == 401) {
                Auth.logout();
            } else {
                FlashMessage.showError('Ha ocurrido un error, intente nuevamente');
            }
        });
    };

    $scope.loadProfile();

});
