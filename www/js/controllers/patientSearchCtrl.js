angular.module('app.controllers')

.controller('patientSearchCtrl', function($scope, $state, $rootScope, $http, FlashMessage, Auth) {

    $scope.search = {};

    $scope.patientAll = function(){
        $scope.showLoading();

        $http.get($rootScope.endPoint + 'patient?token=' + Auth.token()).
        success(function(response){
            $scope.hideLoading();
            if (response.length > 0) {
                $scope.search = {};
                $state.go('tab.patients', {patients: response});
            } else {
                FlashMessage.showError('No tiene pacientes asignados');
            }
        }).
        error(function(error, errorCode){
            $scope.hideLoading();
            if (errorCode == 401) {
                Auth.logout();
            } else {
                FlashMessage.showGeneralError();
            }
        });
    };

    $scope.patientSearch = function(){
        if ($scope.search.q == undefined) {
            FlashMessage.showError('Debe entrar Nombre, Apellidos o Num. Identidad');
        } else {
            $scope.showLoading();

            $http.post($rootScope.endPoint + 'patient/search', $.extend($scope.search, {token:Auth.token()})).
            success(function(response){
                $scope.hideLoading();
                if (response.length > 0) {
                    $scope.search = {};
                    $state.go('tab.patients', {patients: response});
                } else {
                    FlashMessage.showError('La busqueda no devolvio ningun resultado');
                }
            }).
            error(function(error, errorCode){
                $scope.hideLoading();
                if (errorCode == 401) {
                    Auth.logout();
                } else {
                    FlashMessage.showError('Ha ocurrido un error, intente nuevamente');
                }
            });
        }
    };

    Auth.checkLoggedOnline();

});
