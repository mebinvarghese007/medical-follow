angular.module('app.controllers', [])
.controller('loginCtrl', function($scope, $state,endPoint, $rootScope, $location, Session, FlashMessage, $http) {
    $scope.endPoint=endPoint;
    $scope.user = {};

    $scope.login = function(){
        $http.post($rootScope.endPoint + 'auth/login', $scope.user).
        success(function(res){
            $scope.hideLoading();
            $scope.user = {};
            if (res.token) {
                Session.set('promed-token', res.token);
                Session.set('promed-erp', res.is_erp);
                if (res.is_erp) {
                    $state.go('tab_erp.home')
                } else {
                    $state.go('tab.home');
                }
            } else {
                FlashMessage.showError('Ha ocurrido un error, intente nuevamente');
            }
        }).
        error(function(error){
            $scope.hideLoading();
            if (error && typeof error.message != 'undefined') {
                FlashMessage.showError(error.message);
            } else {
                FlashMessage.showError('Ha ocurrido un error, intente nuevamente');
            }
        });
    };

    $scope.forgotPassword = function(){
        $state.go('forgot_password');
    }

})
.controller('forgotPasswordCntrl', function($scope, $state,endPoint, $rootScope, $location, Session, FlashMessage, $http) {
 

 $scope.user = {};
 $scope.updatePassword = function(){ 
    if($scope.user.email != ''){

        $scope.showLoading();
            $http.post($rootScope.endPoint + 'auth/forget-password', $scope.user).
            success(function(res){
                $scope.hideLoading();
                console.log(res);
                FlashMessage.showInformation('contraseña enviada a su dirección de correo electrónico registrada',function(){
                    $state.go('login');
                });
            }).
            error(function(error){
                $scope.hideLoading();

                console.log(error);
                if (error && typeof error.message != 'undefined') {
                    FlashMessage.showError('nombre de usuario no válido');
                } else {
                    FlashMessage.showError('Ha ocurrido un error, intente nuevamente');
                }
            });  
    } 
 }

});
