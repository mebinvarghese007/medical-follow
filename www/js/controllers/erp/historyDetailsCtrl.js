angular.module('app.controllers')

.controller('historyDetailsCtrl', function($scope, $ionicPopup, $timeout, $stateParams,$state, $rootScope, $http, FlashMessage, Auth) {
	console.log($stateParams);

	$scope.historyData = $stateParams.historyData;
	$scope.section = $stateParams.type;
  console.log(angular.fromJson($stateParams.images));
  $scope.images = [];
  $scope.images = angular.fromJson($stateParams.images);
	angular.forEach($scope.historyData, function(value,key){
		var data = {
			value:'',
			key:''
		};
        if(value.value == 1 && value.key=="tipo"){
          data.value  = 'Osteosíntesis';
          data.key = 'Tipo'
          $scope.historyData.splice(0, 0, data);
		  var index = $scope.historyData.indexOf(value);
		  $scope.historyData.splice(index, 1); 
        }
        if(value.value == 2  && value.key=="tipo"){
           data.value  = 'Prótesis';
           data.key = 'Tipo'
           $scope.historyData.splice(0, 0, data);
		  	var index = $scope.historyData.indexOf(value);
		  	$scope.historyData.splice(index, 1); 
        }
        if(value.value == 3 && value.key=="tipo"){
           data.value  = 'Artroscopia';
           data.key = 'Tipo'
           $scope.historyData.splice(0, 0, data);
		  var index = $scope.historyData.indexOf(value);
		  $scope.historyData.splice(index, 1); 
        }
        
    })

    $scope.historyData1 = $scope.historyData;
});
