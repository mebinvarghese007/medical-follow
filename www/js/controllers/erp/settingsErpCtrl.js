angular.module('app.controllers')

.controller('settingsErpCtrl', function($scope, $state, $timeout,offlineWebservice, $rootScope, $http, $filter, FlashMessage, Auth, ionicDatePicker,$localStorage) {

    $scope.profile = {};
    $scope.birth_date = new Date(1980, 0, 1);
    $scope.apiData = {};
    $scope.passValid = false;

    var inputDateParams = {
        callback: function (val) {  //Mandatory
            var date = new Date(val),
                day = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()),
                month = (date.getMonth() + 1),
                month = (month < 10 ? '0' + month : month);

            $scope.profile.doctor.birth_date = date.getFullYear() + '-' + month + '-' + day;
            $scope.birth_date = new Date(val);
        },
        setLabel: 'Seleccionar',
        closeOnSelect: false,
        from: new Date(1919, 01, 01), //Optional
        to: new Date(2016, 10, 30), //Optional
    };

    $scope.openDatePicker = function(){
        inputDateParams.inputDate = $scope.birth_date;
        ionicDatePicker.openDatePicker(inputDateParams);
    };

    $scope.loadProfile = function(){
        $scope.showLoading();

        $http.get($rootScope.endPoint + 'auth/profile?token=' + Auth.token()).
        success(function(response){
            $scope.hideLoading();
            $scope.profile = response;
            $scope.profile.doctor.password = '';
            $localStorage.profileData = response;
            // console.log($localStorage.profileData);

            if (typeof response.doctor.birth_date_parts != 'undefined') {
                var birth_date = response.doctor.birth_date_parts;
                $scope.birth_date = new Date(birth_date[0], (birth_date[1] * 1) - 1, birth_date[2]);//new Date(response.doctor.birth_date);
            } else {
                $scope.profile.doctor.birth_date = 'Ninguna';
            }
        }).
        error(function(error, errorCode){
            console.log(errorCode);
            if (errorCode == 401) {
                $scope.hideLoading();
                Auth.logout();
            } else {
                if(!_.isEmpty($localStorage.profileData)){
                            $timeout(function () {
                                $scope.hideLoading();
                            }, 1000);
                    $scope.profile = $localStorage.profileData;
                    if (typeof $localStorage.profileData.doctor.birth_date_parts != 'undefined') {
                        var birth_date = $localStorage.profileData.doctor.birth_date_parts;
                        $scope.birth_date = new Date(birth_date[0], (birth_date[1] * 1) - 1, birth_date[2]);//new Date(response.doctor.birth_date);
                    } else {
                        $scope.profile.doctor.birth_date = 'Ninguna';
                    }
                }
                else{
                    $scope.hideLoading();
                    FlashMessage.showError('Ha ocurrido un error, intente nuevamente');
                }

            }
        });
    };
    
    $scope.updateProfile = function(form) {
        if(form){
            if($scope.profile.doctor.password != ''){
                if($scope.profile.doctor.password.length>7){
                    $scope.passValid = false;
                    $scope.showLoading();
                    console.log($scope.profile.doctor)

                    $http.post($rootScope.endPoint + 'auth/profile?token=' + Auth.token(), $scope.profile.doctor).
                    success(function(response){
                        $scope.hideLoading();
                        if (response.success) {
                            if(response.message){
                                FlashMessage.show('Clave modificada con éxito');
                                $scope.profile.doctor.password = '';
                            }
                            else{
                                FlashMessage.showInformation('La información de perfil ha sido actualizada');
                            }
                        } else {
                            FlashMessage.showError('Ha ocurrido un error, por favor intente nuevamente');
                        }
                    }).
                    error(function(error, errorCode){
                        $timeout(function () {
                            $scope.hideLoading();
                            if (errorCode == 401) {
                                Auth.logout();
                            } else {
                                 $scope.apiData.api = $rootScope.endPoint + 'auth/profile?token=' + Auth.token();
                                 $scope.apiData.data = $scope.profile.doctor;
                                 $scope.apiData.type = 'update_profile';
                                 offlineWebservice.storeApiRequest($scope.apiData);
                                FlashMessage.showInformation('Usted está fuera de línea, la información se sincronizará cuando haya conexión de datos');
                            }
                        }, 1000);
                    }); 

                }
                else{
                    console.log("Enter a valid password");
                     $scope.passValid = true;
                }
            }  
            else{

                $scope.showLoading();
                console.log($scope.profile.doctor)

                $http.post($rootScope.endPoint + 'auth/profile?token=' + Auth.token(), $scope.profile.doctor).
                success(function(response){
                    $scope.hideLoading();
                    if (response.success) {
                        if(response.message){
                            FlashMessage.show('Clave modificada con éxito');
                        }
                        else{
                            FlashMessage.showInformation('La información de perfil ha sido actualizada');
                        }
                    } else {
                        FlashMessage.showError('Ha ocurrido un error, por favor intente nuevamente');
                    }
                }).
                error(function(error, errorCode){
                    $timeout(function () {
                        $scope.hideLoading();
                        if (errorCode == 401) {
                            Auth.logout();
                        } else {
                             $scope.apiData.api = $rootScope.endPoint + 'auth/profile?token=' + Auth.token();
                             $scope.apiData.data = $scope.profile.doctor;
                             $scope.apiData.type = 'update_profile';
                             offlineWebservice.storeApiRequest($scope.apiData);
                            FlashMessage.showInformation('Usted está fuera de línea, la información se sincronizará cuando haya conexión de datos');
                        }
                    }, 1000);
                }); 
            }
        }
    };

    $scope.loadProfile();

    $scope.logout = function() {
        Auth.logout();
    };

     $scope.$watch("profile.doctor.password", function(newValue, oldValue){
                   if(newValue){
                         if(newValue.length > 7){
                            $scope.passValid = false;
                        } 
                   }  
          });
    // $scope.showHistory = function(){
    //     $state.go('tab_erp.history');
    // }

});
