angular.module('app.controllers')

.controller('homeErpCtrl', function($scope, $ionicPopup, $timeout, offlineWebservice,$localStorage,$state, $rootScope, $http, FlashMessage, Auth) {
    $scope.passError = false;
    $scope.profile = {};
    $scope.loadProfile = function(){
        $scope.showLoading();

        $http.get($rootScope.endPoint + 'auth/profile?token=' + Auth.token()).
        success(function(response){
            $scope.hideLoading();
            $scope.profile = response;
            console.log(response);
            var created_at = Date.parse(response.created_at);
            var updated_at = Date.parse(response.updated_at);
            
            if(!window.localStorage.getItem('skip')){
                if(created_at === updated_at){
                    $scope.showPopup();
                }
            }
            offlineWebservice.updateApiRequest();
            $localStorage.homeProfileData = response;
        }).
        error(function(error, errorCode){
            if (errorCode == 401) {
                Auth.logout();
                $scope.hideLoading();
            } else {
                if(!_.isEmpty($localStorage.homeProfileData)){
                    $scope.profile = $localStorage.homeProfileData;
                             $timeout(function () {
                                $scope.hideLoading();
                            }, 1000);
                }
                else{
                    $scope.hideLoading();
                    FlashMessage.showError('Ha ocurrido un error, intente nuevamente');
                }
                // FlashMessage.showError('Falla de internet');
            }
        });
    };

    $scope.showPopup = function() {
          $scope.data = {}
        
          // Custom popup
          var myPopup = $ionicPopup.show({
             template: '<input type = "password" ng-model = "profile.password"><span ng-show="passError" class="assertive">La contraseña debe ser de 8 letras<span>',
             title: 'Modificar Clave',
             subTitle: 'Por favor modifique la contraseña enviada a su correo.',
             scope: $scope,
                
             buttons: [
                { text: 'Omitir'
                     }, {
                   text: '<b>Guardar</b>',
                   type: 'button-positive',
                   onTap: function(e) {
                            
                      if (!$scope.profile.password) {
                         e.preventDefault();
                      } else {
                            console.log($scope.profile.password.length);
                                var pasLength = $scope.profile.password.length;
                                if(pasLength>=8){
                                    $scope.passError = false;
                                    $scope.changePassword($scope.profile.password);
                                }
                                else{
                                    $scope.passError = true;
                                    e.preventDefault();
                                }
                         return $scope.profile.password;
                      }
                   }
                }
             ]
          });

          myPopup.then(function(res) {
             if(res){
                 console.log('Tapped!', res);
             }
             else{
                 console.log('Tapped!', res);
                window.localStorage.setItem('skip', true)
             }
          });    
       };


       $scope.changePassword = function(pas){
            console.log(pas);
            $scope.showLoading();
            $http.post($rootScope.endPoint + 'auth/password?token=' + Auth.token(), $scope.profile).
            success(function(response){
                console.log(response);
                FlashMessage.show('éxito','Clave modificada');
                $scope.hideLoading();
            })
            .error(function(error,errorCode){
                if (errorCode == 401) {
                Auth.logout();
                $scope.hideLoading();
            } else {
                    $scope.hideLoading();
                    FlashMessage.showError('Ha ocurrido un error, intente nuevamente');
            }
            })
       }

    $scope.loadProfile();

});
