angular.module('app.controllers')

.controller('patientErpSearchCtrl', function($scope, offlinedata, $timeout, $localStorage,$state, $rootScope, $http, FlashMessage, Auth, $ionicPopup, $location) {

    $scope.search = {};

    $scope.patientAll = function(){
        $scope.showLoading();

        $http.get($rootScope.endPoint + 'erp/patient?token=' + Auth.token()).
        success(function(response){
            $scope.hideLoading();
            if (response.length > 0) {
                $scope.search = {};
                $state.go('tab_erp.patients', {patients: response});
            } else {
                FlashMessage.showError('No tiene pacientes asignados');
            }
        }).
        error(function(error, errorCode){
            $scope.hideLoading();
            if (errorCode == 401) {
                Auth.logout();
            } else {
                FlashMessage.showGeneralError();
            }
        });
    };

    $scope.patientSearch = function(){
        if ($scope.search.q == undefined) {
            FlashMessage.showError('Debe entrar Nombre, Apellidos o Num. Identidad');
        } else {
            $scope.showLoading();

            $http.post($rootScope.endPoint + 'erp/patient/search', $.extend($scope.search, {token:Auth.token()})).
            success(function(response){
                $scope.hideLoading();
                if (response.length > 0) {
                    $scope.search = {};
                    $state.go('tab_erp.patients', {patients: response});
                    offlinedata.storeSearchResult(response);
                } else {
                    $scope.showNoResults();
                }
            }).
            error(function(error, errorCode){
                if (errorCode == 401) {
                    $scope.hideLoading();
                    Auth.logout();
                } else {
                    var data = offlinedata.searchResult($scope.search.q);
                    if(data.length>0){
                             $timeout(function () {
                                $scope.hideLoading();

                                 $state.go('tab_erp.patients', {patients: data}); 
                            }, 1000); 
                    }
                    else{
                        $scope.hideLoading();
                        FlashMessage.showError('Se ha producido un error, vuelva a intentarlo y no tiene datos anteriores a la lista',function(){
                                $scope.showNoResults();  
                        });
                                              
                    }

                }
            });
        }
    };

    $scope.showNoResults = function() {
        $ionicPopup.confirm({
            title: 'Información',
            template: 'Paciente no Encontrado',
            buttons: [
                {
                    text: 'Aceptar',
                    type: 'button-positive'
                },
                {
                    text: 'Crear Paciente',
                    type: 'button-positive',
                    onTap: function() {
                        $location.path('/tab_erp/patient/create');
                    }
                }
            ]
        });
    };

    // Auth.checkLoggedOnline();

})

.controller('patientHistoryErpSearchCtrl', function($scope, offlinedata, $timeout, $localStorage,$state, $rootScope, $http, FlashMessage, Auth, $ionicPopup, $location) {

    $scope.search = {};

    $scope.patientAll = function(){
        $scope.showLoading();

        $http.get($rootScope.endPoint + 'erp/patient?token=' + Auth.token()).
        success(function(response){
            $scope.hideLoading();
            if (response.length > 0) {
                $scope.search = {};
                $state.go('tab_erp.patients', {patients: response});
            } else {
                FlashMessage.showError('No tiene pacientes asignados');
            }
        }).
        error(function(error, errorCode){
            $scope.hideLoading();
            if (errorCode == 401) {
                Auth.logout();
            } else {
                FlashMessage.showGeneralError();
            }
        });
    };

    $scope.patientSearch = function(){
        if ($scope.search.q == undefined) {
            FlashMessage.showError('Debe entrar Nombre, Apellidos o Num. Identidad');
        } else {
            $scope.showLoading();

            $http.post($rootScope.endPoint + 'erp/patient/search', $.extend($scope.search, {token:Auth.token()})).
            success(function(response){
                $scope.hideLoading();
                if (response.length > 0) {
                    $scope.search = {};
                    $state.go('tab_erp.patients_history', {patients: response});  
                    offlinedata.storeSearchResult(response);
                } else {
                    $scope.showNoResults();
                }
            }).
            error(function(error, errorCode){
                if (errorCode == 401) {
                    $scope.hideLoading();
                    Auth.logout();
                } else {
                    var data = offlinedata.searchResult($scope.search.q);
                    if(data.length>0){
                             $timeout(function () {
                                $scope.hideLoading();

                                 $state.go('tab_erp.patients_history', {patients: data}); 
                            }, 1000); 
                    }
                    else{
                        $scope.hideLoading();
                        FlashMessage.showError('Se ha producido un error, vuelva a intentarlo y no tiene datos anteriores a la lista',function(){
                                $scope.showNoResults();  
                        });
                                              
                    }

                }
            });
        }
    };

    $scope.showNoResults = function() {
        $ionicPopup.confirm({
            title: 'Información',
            template: 'Paciente no Encontrado',
            buttons: [
                {
                    text: 'Aceptar',
                    type: 'button-positive'
                },
                {
                    text: 'Crear Paciente',
                    type: 'button-positive',
                    onTap: function() {
                        $location.path('/tab_erp/patient/create');
                    }
                }
            ]
        });
    };

    // Auth.checkLoggedOnline();

});
