angular.module('app.controllers')

.controller('historyCtrl', function($scope, $ionicPopup,offlinedata, $stateParams, $timeout, $localStorage,$state, $rootScope, $http, FlashMessage, Auth) {



        $scope.patientData = [];
        console.log($stateParams)
        $scope.id = $stateParams.id;
        $rootScope.showLoading();
        $http.get($rootScope.endPoint+'patient/record/'+ $scope.id +'?token='+Auth.token())
        .success(function(data){
            $scope.response = $scope.setValues(data);   
             offlinedata.storeHistoryData($scope.response, $scope.id);
             $rootScope.hideLoading();
        })
        .error(function(error,errorCode){
            if (errorCode == 401) {
                  $scope.hideLoading();
                  Auth.logout();
              } else {
                $rootScope.hideLoading();
                $scope.response =  $scope.setValues(offlinedata.getHistoryData($scope.id));
                // FlashMessage.showError('Ha ocurrido un error, intente nuevamente');
              }
        });

        $scope.dataSHow = function(data){
                        console.log(data.images)
          data.data = angular.fromJson(data.data);
                angular.forEach(data.data, function(value,key){
                    if(value!=""){
                        if(value == 'true' || value == 'on'){
                          value = 'Si';
                        }
                        if(value == 'false'){
                          value = 'No';
                        }
                        console.log(data.data)
                        $scope.patientData.push({'key' : key,'value' : value});
                    }
                    
                })

                console.log($scope.patientData);
                $state.go('tab_erp.history_details',{historyData : $scope.patientData,type : data.section_type, images : data.images});


        }

        $scope.newDate = function(value){
          return new Date(value);
        }

        $scope.setValues = function(data){
          angular.forEach(data,function(value,key){
                if(value.type ===2 ){
                  value.type_name = 'Cirugia';
                }else if(value.type ===3 ){
                  value.type_name = 'Control';
                }
                else if(value.type ===1 ){
                      value.type_name = 'Consulta';
                    }
                    else{
                      value.type_name = 'undefined'
                    }
                if(value.section_type ===2 ){ 
                  value.section_name = 'Rodilla';
                }else if(value.section_type ===1 ){
                  value.section_name = 'Cadera';
                }
                else if(value.section_type ===3 ){
                      value.section_name = 'Hombro';
                    }
                    else if(value.section_type ===4 ){
                      value.section_name = 'Columna'
                    }
                    else{
                      value.section_name = 'undefined';
                    }
             })
          return data;
        }

});
