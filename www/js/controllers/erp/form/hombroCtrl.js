angular.module('app.controllers')

.controller('ERP.hombroConsultaCtrl', function($scope, $state, $rootScope, $controller, $stateParams, $http, $sce, FlashMessage, Auth, $ionicActionSheet, ImageService, $timeout) {

    $controller('formCtrl', {
        $scope: $scope,
        $state: $state,
        $rootScope: $rootScope,
        $stateParams: $stateParams,
        $http: $http,
        $sce: $sce,
        $ionicActionSheet: $ionicActionSheet,
        $timeout: $timeout
    });

    $scope.init('hombro/consulta', 'Hombro / Consulta');
})

.controller('ERP.hombroCirugiaCtrl', function($scope, $state, $rootScope, $controller, $stateParams, $http, $sce, FlashMessage, Auth, $ionicActionSheet, ImageService, $timeout) {
    $scope.record_tipo = null;
    $controller('formCtrl', {
        $scope: $scope,
        $state: $state,
        $rootScope: $rootScope,
        $stateParams: $stateParams,
        $http: $http,
        $sce: $sce,
        $ionicActionSheet: $ionicActionSheet,
        $timeout: $timeout
    });
    $scope.init('hombro/cirugia', 'Hombro / Cirugía');
})

.controller('ERP.hombroControlCtrl', function($scope, $state, $rootScope, $controller, $stateParams, $http, $sce, FlashMessage, Auth, $ionicActionSheet, ImageService, $timeout) {

    $controller('formCtrl', {
        $scope: $scope,
        $state: $state,
        $rootScope: $rootScope,
        $stateParams: $stateParams,
        $http: $http,
        $sce: $sce,
        $ionicActionSheet: $ionicActionSheet,
        $timeout: $timeout
    });

    $scope.init('hombro/control', 'Hombro / Control');
});
