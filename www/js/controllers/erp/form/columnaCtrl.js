angular.module('app.controllers')

.controller('ERP.columnaConsultaCtrl', function($scope, $state, $rootScope, $controller, $stateParams, $http, $sce, FlashMessage, Auth, $ionicActionSheet, ImageService, $timeout) {

    $controller('formCtrl', {
        $scope: $scope,
        $state: $state,
        $rootScope: $rootScope,
        $stateParams: $stateParams,
        $http: $http,
        $sce: $sce,
        $ionicActionSheet: $ionicActionSheet,
        $timeout: $timeout
    });

    $scope.init('columna/consulta', 'Columna / Consulta');
})

.controller('ERP.columnaCirugiaCtrl', function($scope, $state, $rootScope, $controller, $stateParams, $http, $sce, FlashMessage, Auth, $ionicActionSheet, ImageService, $timeout) {
    $scope.record_tipo = null;
    $controller('formCtrl', {
        $scope: $scope,
        $state: $state,
        $rootScope: $rootScope,
        $stateParams: $stateParams,
        $http: $http,
        $sce: $sce,
        $ionicActionSheet: $ionicActionSheet,
        $timeout: $timeout
    });
    $scope.init('columna/cirugia', 'Columna / Cirugía');
})

.controller('ERP.columnaControlCtrl', function($scope, $state, $rootScope, $controller, $stateParams, $http, $sce, FlashMessage, Auth, $ionicActionSheet, ImageService, $timeout) {

    $controller('formCtrl', {
        $scope: $scope,
        $state: $state,
        $rootScope: $rootScope,
        $stateParams: $stateParams,
        $http: $http,
        $sce: $sce,
        $ionicActionSheet: $ionicActionSheet,
        $timeout: $timeout
    });

    $scope.init('columna/control', 'Columna / Control');
});
