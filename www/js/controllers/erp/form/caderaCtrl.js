angular.module('app.controllers')

        .controller('ERP.caderaConsultaCtrl', function ($scope, $state, $rootScope, $controller, $stateParams, $http, $sce, FlashMessage, Auth, $ionicActionSheet, ImageService, $timeout) {

            $controller('formCtrl', {
                $scope: $scope,
                $state: $state,
                $rootScope: $rootScope,
                $stateParams: $stateParams,
                $http: $http,
                $sce: $sce,
                $ionicActionSheet: $ionicActionSheet,
                $timeout: $timeout
            });

            $scope.init('cadera/consulta', 'Cadera / Consulta');
        })

        .controller('ERP.caderaCirugiaCtrl', function ($scope, $state, $rootScope, $controller, $stateParams, $http, $sce, FlashMessage, Auth, $ionicActionSheet, ImageService, $timeout) {

            $scope.record_tipo = null;
            $scope.showLoading();
            $controller('formCtrl', {
                $scope: $scope,
                $state: $state,
                $rootScope: $rootScope,
                $stateParams: $stateParams,
                $http: $http,
                $sce: $sce,
                $ionicActionSheet: $ionicActionSheet,
                $timeout: $timeout
            });
            $scope.init('cadera/cirugia', 'Cadera / Cirugía');
        })

        .controller('ERP.caderaControlCtrl', function ($scope, $state, $rootScope, $controller, $stateParams, $http, $sce, FlashMessage, Auth, $ionicActionSheet, ImageService, $timeout) {

            $controller('formCtrl', {
                $scope: $scope,
                $state: $state,
                $rootScope: $rootScope,
                $stateParams: $stateParams,
                $http: $http,
                $sce: $sce,
                $ionicActionSheet: $ionicActionSheet,
                $timeout: $timeout
            });

            $scope.init('cadera/control', 'Cadera / Control');
        });
