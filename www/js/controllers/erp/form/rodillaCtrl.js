angular.module('app.controllers')

.controller('ERP.rodillaConsultaCtrl', function($scope, $state, $rootScope, $controller, $stateParams, $http, $sce, FlashMessage, Auth, $ionicActionSheet, ImageService, $timeout) {

    $controller('formCtrl', {
        $scope: $scope,
        $state: $state,
        $rootScope: $rootScope,
        $stateParams: $stateParams,
        $http: $http,
        $sce: $sce,
        $ionicActionSheet: $ionicActionSheet,
        $timeout: $timeout
    });

    $scope.init('rodilla/consulta', 'Rodilla / Consulta');
})

.controller('ERP.rodillaCirugiaCtrl', function($scope, $state, $rootScope, $controller, $stateParams, $http, $sce, FlashMessage, Auth, $ionicActionSheet, ImageService, $timeout) {

    $scope.record_tipo = null;

    $controller('formCtrl', {
        $scope: $scope,
        $state: $state,
        $rootScope: $rootScope,
        $stateParams: $stateParams,
        $http: $http,
        $sce: $sce,
        $ionicActionSheet: $ionicActionSheet,
        $timeout: $timeout
    });

    $scope.init('rodilla/cirugia', 'Rodilla / Cirugía');
})

.controller('ERP.rodillaControlCtrl', function($scope, $state, $rootScope, $controller, $stateParams, $http, $sce, FlashMessage, Auth, $ionicActionSheet, ImageService, $timeout) {

    $controller('formCtrl', {
        $scope: $scope,
        $state: $state,
        $rootScope: $rootScope,
        $stateParams: $stateParams,
        $http: $http,
        $sce: $sce,
        $ionicActionSheet: $ionicActionSheet,
        $timeout: $timeout
    });

    $scope.init('rodilla/control', 'Rodilla / Control');
});
