angular.module('app.controllers')

        .controller('formCtrl', function ($scope, $state, $localStorage, offlineWebservice, ScoreResult,offlinedata, $rootScope, $controller, $stateParams, $http, $sce, FlashMessage, Auth, $ionicActionSheet, ImageService, $timeout, ionicTimePicker, $ionicModal, $ionicScrollDelegate, $cordovaCamera) {

            $scope.stage = {};
            $scope.indexActive = 1;
            $scope.sections = {};
            $scope.sectionName = null;
            $scope.currentQuestion = {};
            $scope.question = {};
            $scope.images = {};
            $scope.points = [];
            $scope.apiData = {};
            $scope.enfermedad = false;
            $scope.deformidad = false;
            $scope.espondilolistesis = false;
            $scope.fractura_truama = false;
            $scope.tumor = false;
            $scope.inflamacion = false;
            $scope.infeccion = false;
            $scope.reintervencion = false;   
            $scope.segun_segmento = true;
            $scope.imagenStatus = false;         
            $scope.formQuestion = [
                {
                    'region': {},
                    'procedimiento': {},
                    'fabricante': {},
                    'tecnica': {},
                    'implante': {},
                    'nombre_comercial': {},
                    'sub_segment': {}
                }
            ];
            $scope.formQuestionNew = [
                {
                    'procedimiento': {},
                    'fabricante': {},
                    'tecnica': {},
                    'implante': {},
                    'nombre_comercial': {},
                    'sub_segment': {}
                }
            ];
            $scope.nombresComercialesListNew = [];
            $scope.cirugia_protesis = [];
            $scope.artroscopia = [];
            $scope.ui = {cancelButton: 'Cancelar'};
            $scope.pageTitle = '';
            $scope.ifHappened = '';
            $scope.originalPageTitle = '';
            $scope.currentDate = new Date();
            $scope.currentTime = new Date('HH:mm');
            $scope.shower = 1;

            $scope.escapulTreatmento = [{
                id:"placa",
                name:"Placa"
            },{
                id:"tornillos",
                name:"Tornillos"
            }];

            $scope.rotulaTreatmento = [{
                id:"placa",
                name:"Placa"
            },{
                id:"alambre",
                name:"Alambre"
            },{
                id:"alambre mas tornillos",
                name:"Alambre más tornillos"
            },{
                id:"Alambre mas kirschner",
                name:"Alambre más Kirschner"
            },{
                id:"tornillos",
                name:"Tornillos"
            }];

            $scope.defaultTreatmento = [{
                id:"placa",
                name:"Placa"
            },{
                id:"clavo endomedular",
                name:"Clavo endomedular"
            },{
                id:"fijador externo",
                name:"Fijador externo"
            },{
                id:"tornillos",
                name:"Tornillos"
            }];


            $ionicModal.fromTemplateUrl('templates/erp/form/oa-foundation.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modal = modal;
            });

            $scope.openModal = function (question) {
                $scope.modal.show();
            };

            $scope.closeModal = function(val){
                $scope.showBoat();
                $scope.record_tipo = val
                $scope.recordTipoSelected(val);
            }
            // Load form
            $scope.init = function (section, pageTitle) {
                var oldSoftBack = $rootScope.$ionicGoBack;
                if (section.indexOf("cirugia") >= 0) {
                    $rootScope.$ionicGoBack = function () {
                        if (section.indexOf("cirugia") >= 0 && $scope.ifHappened != '')
                        {
                            $scope.shower = 0;
                            $scope.ifHappened = '';
                        }
                        $rootScope.$ionicGoBack = oldSoftBack;
                    }
                }
                $scope.url = $rootScope.endPoint + 'erp/form/' + section + '/' + $stateParams.patientId + '?token=' + Auth.token();
                $scope.pageTitle = pageTitle;
                console.log(pageTitle);
                $scope.originalPageTitle = pageTitle;
                $scope.showLoading();
                var sect1 = {id: 1, name: "Osteosíntesis"};
                var sect2 = {id: 2, name: "Prótesis"};
                var sect3 = {id: 3, name: "Artroscopia"};
                var sections = [sect1, sect2, sect3];
                $scope.sections = sections;
                $http.get($scope.url).success(function (response) {
                        $scope.hideLoading();
                        $scope.sections = response.sections;
                        $scope.response = response;
                        $scope.dataUpdate(section);
                        offlinedata.storeMedicalData(section,response);
                }).error(function (error, errorCode) {
                    if (errorCode == 401) {
                        $scope.hideLoading();
                        Auth.logout();
                    } else {
                            $timeout(function () {
                                $scope.hideLoading();
                                var offlineDataResult = offlinedata.getMedicalData(section);
                                if(!_.isEmpty(offlineDataResult)){
                                    $scope.sections = offlineDataResult.sections;
                                    $scope.response = offlineDataResult;
                                    // $scope.dataUpdate(section);
                                }
                                else{
                                    $scope.hideLoading();
                                    FlashMessage.showError(error != null && typeof error.message != 'undefined' ? error.message : 'Ha ocurrido un error, intente nuevamente & no tienes datos previamente cargados', function () {
                                        $state.go('tab_erp.patient', {patientId: $stateParams.patientId});
                                    });   
                                }
                            }, 1000);
                    }
                });
            };
            
            $scope.addMedia = function (questionId) {
                $scope.hideSheet = $ionicActionSheet.show({
                    titleText: 'Agregar Imagen',
                    buttons: [
                        {text: 'Tomar Foto'},
                        {text: 'Escoger de la Galería'}
                    ],
                    cancelText: 'Cancelar',
                    buttonClicked: function (index) {
                        $scope.addImage(index, questionId);
                    }
                });
            };
            $scope.addImage = function (type, questionId) {
                $scope.hideSheet();
                $scope.showLoading();
                ImageService.handleMediaDialog(type, function (err, response) {
                    $scope.showLoading();
                    console.log(err);
                    console.log(response);
                    if (!err) {
                        $('#images_' + questionId).val(response);
                        $scope.images[questionId] = response;
                        $scope.hideLoading();
                        FlashMessage.showInformation('La imagen se ha guardado');

                        if ($scope.imimq == questionId) {
                            $scope.imim = questionId + 'Image';
                            $scope.imimq = questionId;
                        }
                        if ($scope.imimq != questionId) {
                            $scope.imimOld = $scope.imim;
                            $scope.imim = questionId + 'Image';
                            $scope.imimq = questionId;
                        }

                        $scope.imagenStatus = true;
                    }
                    else{
                        $scope.hideLoading();
                        FlashMessage.showError('Algo salió mal. Por favor, vuelva a intentarlo');
                    }
                    //$scope.$apply();
                    
                });
            };
            $scope.save = function (qwerty) {
                var sendData = ''
                if($scope.pageTitle.toLowerCase().indexOf("cirugía") >= 0){
                    sendData = $('#frmErpForms').serialize();
                }
                else{
                    sendData = $('#frmErpForms').serialize()+ '&question%5Bscore%5D='+ $scope.totalConsultaPoints().score+ '&question%5Bstatus%5D='+ $scope.totalConsultaPoints().statusValue;
                }
                console.log(sendData);
                if($scope.imagenStatus){

                    $scope.showLoading();
                    // console.log(JSON.stringify($('#frmErpForms').serialize()));
                    $.post($scope.url, JSON.stringify(sendData), function (response) {
                        //console.log(response);

                        $scope.hideLoading();

                        if ($scope.pageTitle.toLowerCase().indexOf("cirugía") >= 0)
                            {
                                FlashMessage.showInformation('El formulario del paciente ha sido guardado', function () {
                                    $scope.indexActive = 1;
                                    $state.go('tab_erp.patient', {patientId: $stateParams.patientId});
                                });
                            }
                            else{
                                    if($scope.pageTitle.toLowerCase().indexOf("columna") >= 0){
                                            $scope.scoreTitle = 'Severidad';
                                    }
                                    else{
                                         $scope.scoreTitle = 'Total Escala';
                                    }
                                FlashMessage.showEscala($scope.scoreTitle,'Total Puntaje </br>'+$scope.totalConsultaPoints().status,function(){

                                    FlashMessage.showInformation('El formulario del paciente ha sido guardado', function () {
                                        $scope.indexActive = 1;
                                        $state.go('tab_erp.patient', {patientId: $stateParams.patientId});
                                    });
                                })
                            }
                    }).error(function () {
                                $timeout(function () {
                                    $scope.hideLoading();
                                    $scope.apiData.api = $scope.url;
                                    $scope.apiData.data = $('#frmErpForms').serialize();
                                    $scope.apiData.type = 'form';
                                    offlineWebservice.storeApiRequest($scope.apiData);
                                    falseshMessage.showInformation    ('Usted está fuera de línea, la información se sincronizará cuando haya conexión de datos',function(){
                                                    $scope.indexActive = 1;
                                                    $state.go('tab_erp.patient', {patientId: $stateParams.patientId});
                                    });
                                }, 1000);
                    });
                }
                else{
                    FlashMessage.showError('Cargue al menos una imagen');
                }
            };
            $scope.renderHtml = function (html_code) {
                return $sce.trustAsHtml(html_code);
            };
            $scope.displayIf = function (index) {
                return index == $scope.indexActive;
            };
            $scope.previousSection = function () {
                $scope.indexActive--;
                $ionicScrollDelegate.scrollTop();
            };
            $scope.nextSection = function () {
                $scope.indexActive++;
                $ionicScrollDelegate.scrollTop();
            };
            $scope.displayIfFirst = function (index) {
                return index == 1 && $scope.sections.length > 1
            };
            $scope.displayIfMiddle = function (index) {
                return index > 1 && index < $scope.sections.length;
            };
            $scope.displayIfLast = function (index) {
                return index == $scope.sections.length;
            };
            $scope.isUndefined = function (thing) {
                return (typeof thing === "undefined");
            };
            $scope.refreshQuestions = function () {
                $('[data-require-id]').each(function () {
                    var questionId = $(this).data('requireId'),
                            answers = ($(this).data('requireAnswers') + '').split(",");
                    if (questionId > 0 && answers.length > 0) {
                        var $requiredQuestion = $('[name="answers[' + questionId + ']"]'),
                                displayField = false;
                        //console.log($(this).data("name") + ' ' + answers + ' => ' + $requiredQuestion.val() + ' => ' + answers.indexOf($requiredQuestion.val()));

                        // Is Normal field?
                        if ($requiredQuestion.length > 0) {
                            if (answers.indexOf($requiredQuestion.val()) != -1) {
                                displayField = true;
                            }
                        } else {
                            // Is Checkbox field?
                            $requiredQuestion = $('[name="answers[' + questionId + '][]"]');
                            if ($requiredQuestion.length > 0) {
                                $requiredQuestion.each(function () {
                                    if (answers.indexOf($(this).val()) != -1 && $(this).is(":checked")) {
                                        displayField = true;
                                    }
                                });
                            }
                        }

                        if (displayField) {
                            $(this).show();
                        } else {
                            $(this).hide();
                        }
                    }
                });
            };
            $scope.cansoleNombre = function(val){
                console.log(val)
            }
            $scope.queueRefreshQuestions = function (id) {
                $timeout($scope.refreshQuestions, 100);
                $scope.resetCups = false;

            };
            $scope.consoleQue = function(val){
                console.log(val)
            }
            $scope.recordTipoSelected = function (val) {
                //console.log(val)
                $scope.showLoading();


                // Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.


                if (val.id == 1)
                {
                    if (typeof $scope.response.cirugia_osteo !== 'undefined')
                        $scope.cirugiaosteo = $scope.response.cirugia_osteo;
                }
                if (val.id == 2)
                {
                    if (typeof $scope.response.cirugia_protesis !== 'undefined')
                        $scope.cirugia_protesis = $scope.response.cirugia_protesis;
                }
                if (val.id == 3)
                {
                    if (typeof $scope.response.cirugia_artroscopia !== 'undefined')
                        $scope.artroscopia = $scope.response.cirugia_artroscopia;
                }

                //console.log()

                $timeout(function () {
                    $scope.hideLoading();
                    if(val.name != undefined){
                        $scope.pageTitle = $scope.originalPageTitle + ' / ' + val.name;
                        $scope.ifHappened = val.name;
                        $scope.shower = 1;
                    }
                    
                    
                }, 1000);
//                 if (typeof $scope.response.sectionName !== 'undefined')
//                        $scope.sectionName = $scope.response.sectionName;

//                    if (typeof $scope.response.cirugia_artroscopia !== 'undefined')
//                        $scope.artroscopia = $scope.response.cirugia_artroscopia;
//                    if (typeof $scope.response.cirugia_osteo !== 'undefined')
//                        $scope.cirugiaosteo = $scope.response.cirugia_osteo;
//                    if (typeof $scope.response.segments !== 'undefined')
//                        $scope.firstSegments = $scope.response.segments;
//                    if (typeof $scope.response.humanPart !== 'undefined')
//                        $scope.humanPart = $scope.response.humanPart;
            };
            $scope.showBoat = function ()
            {
                //console.log($scope.response.cirugia_protesis)
                if (typeof $scope.response.sectionName !== 'undefined')
                    $scope.sectionName = $scope.response.sectionName;
 //              if (typeof $scope.response.cirugia_protesis !== 'undefined')
 //                  $scope.cirugia_protesis = $scope.response.cirugia_protesis;
//                if (typeof $scope.response.cirugia_artroscopia !== 'undefined')
//                    $scope.artroscopia = $scope.response.cirugia_artroscopia;
//                if (typeof $scope.response.cirugia_osteo !== 'undefined')
//                    $scope.cirugiaosteo = $scope.response.cirugia_osteo;
                if (typeof $scope.response.segments !== 'undefined') {
                    $scope.firstSegments = $scope.response.segments;
                }
                if (typeof $scope.response.humanPart !== 'undefined')
                    $scope.humanPart = $scope.response.humanPart;
            }
            $scope.getQuestion = function (sectionId, questionId) {
                return typeof $scope.currentQuestion[sectionId] != 'undefined' && typeof $scope.currentQuestion[sectionId][questionId] != 'undefined'
                        ? $scope.currentQuestion[sectionId][questionId].id
                        : null;
            };
            // ----------------------------------------------------------------
            // TimePicker
            // ----------------------------------------------------------------

            var timeOptions = {
                format: 24, //Optional
                step: 30, //Optional
                setLabel: 'Seleccionar'    //Optional
            };
            $scope.chooseTime = function (id) {
                timeOptions.inputTime = $('#question_time_' + id).data('epoch');
                timeOptions.callback = function (val) {
                    if (typeof (val) === 'undefined') {
                    } else {
                        var selectedTime = new Date(val * 1000),
                                hours = addZero(selectedTime.getUTCHours()),
                                minutes = addZero(selectedTime.getUTCMinutes());
                        $('#question_time_' + id).val(hours + ':' + minutes);
                        $('#question_time_' + id).data('epoch', val);
                    }
                };
                timeOptions.closeLabel = 'Cerrar';
                ionicTimePicker.openTimePicker(timeOptions);
            };
            $scope.timeExists = function (id) {
                return $('#question_time_' + id).val() !== '';
            };
            $scope.getTime = function (id) {
                return $('#question_time_' + id).val();
            };
            var addZero = function (i) {
                if (i < 10) {
                    i = "0" + i;
                }
                return i;
            };

            $scope.cosoleDegen = function(id){
                if(id == 'pathologica_principal'){
                    setTimeout(function(){
                        var inputValue = $('[name="answer[pathologica_principal]"]').val();
                        switch(inputValue){
                            case '1' :  $scope.enfermedad = true;
                                        $scope.deformidad = false;
                                        $scope.espondilolistesis = false;
                                        $scope.fractura_truama = false;
                                        $scope.tumor = false;
                                        $scope.inflamacion = false;
                                        $scope.infeccion = false;
                                        $scope.reintervencion = false;
                                        break;
                            case '2' :  $scope.deformidad = true;
                                        $scope.enfermedad = false;
                                        $scope.espondilolistesis = false;
                                        $scope.fractura_truama = false;
                                        $scope.tumor = false;
                                        $scope.inflamacion = false;
                                        $scope.infeccion = false;
                                        $scope.reintervencion = false;
                                        break;
                            case '3' :  $scope.espondilolistesis = true;
                                        $scope.deformidad = false;
                                        $scope.enfermedad = false;
                                        $scope.fractura_truama = false;
                                        $scope.tumor = false;
                                        $scope.inflamacion = false;
                                        $scope.infeccion = false;
                                        $scope.reintervencion = false;
                                        break;

                            case '4' :  $scope.fractura_truama = true;
                                        $scope.espondilolistesis = false;
                                        $scope.deformidad = false;
                                        $scope.enfermedad = false;
                                        $scope.tumor = false;
                                        $scope.inflamacion = false;
                                        $scope.infeccion = false;
                                        $scope.reintervencion = false;
                                        break;

                            case '5' :  $scope.tumor = true;
                                        $scope.fractura_truama = false;
                                        $scope.espondilolistesis = false;
                                        $scope.deformidad = false;
                                        $scope.enfermedad = false;
                                        $scope.inflamacion = false;
                                        $scope.infeccion = false;
                                        $scope.reintervencion = false;
                                        break;

                            case '6' :  $scope.inflamacion = true;
                                        $scope.tumor = false;
                                        $scope.fractura_truama = false;
                                        $scope.espondilolistesis = false;
                                        $scope.deformidad = false;
                                        $scope.enfermedad = false;
                                        $scope.infeccion = false;
                                        $scope.reintervencion = false;
                                        break;

                            case '7' :  $scope.infeccion = true;
                                        $scope.tumor = false;
                                        $scope.fractura_truama = false;
                                        $scope.espondilolistesis = false;
                                        $scope.deformidad = false;
                                        $scope.enfermedad = false;
                                        $scope.inflamacion = false;
                                        $scope.reintervencion = false;
                                        break;

                            case '8' :  $scope.reintervencion = true;
                                        $scope.infeccion = false;
                                        $scope.tumor = false;
                                        $scope.fractura_truama = false;
                                        $scope.espondilolistesis = false;
                                        $scope.deformidad = false;
                                        $scope.enfermedad = false;
                                        $scope.inflamacion = false;
                                        break;

                            default : break;
                        }
                    },100)
                }

                if(id == 'enfermedad_degenerativa'){
                    setTimeout(function(){
                        var inputValue = $('[name="answer[enfermedad_degenerativa]"]').val();
                        if(inputValue == 11){
                            $scope.enfermedad_otra = true;
                            $scope.espondilolistesis = false;
                            $scope.deformidad = false;   
                        }
                        else if(inputValue == 6){
                                    $scope.deformidad = true;
                                    $scope.enfermedad_otra = false; 
                                    $scope.enfermedad_otra = false;  
                        }
                        else if(inputValue == 10){
                                $scope.espondilolistesis = true;
                                $scope.enfermedad_otra = false;
                                $scope.deformidad = false;
                        }
                        else{
                            $scope.enfermedad_otra = false;
                            $scope.espondilolistesis = false;
                            $scope.deformidad = false;
                        }
                    },100)
                }

                if(id == 'tipo_de_deformidad'){
                    setTimeout(function(){
                        var inputValue = $('[name="answer[tipo_de_deformidad]"]').val();
                        console.log(inputValue);
                        if(inputValue == 4){
                            $scope.deformidad_otra = true; 
                        }
                        else{
                            $scope.deformidad_otra = false;
                        }
                    },100)
                }

                if(id == 'fractura_truama'){
                    setTimeout(function(){
                        var inputValue = $('[name="answer[fractura_truama]"]').val();
                        if(inputValue == 10){
                            $scope.fractura_truama_octra = true;
                            $scope.tipo_de_fractura_1 = false; 
                            $scope.tipo_de_fractura_2 = false; 
                        }
                        else if(inputValue == 5 ){
                            $scope.tipo_de_fractura_1 = true;
                            $scope.fractura_truama_octra = false;
                            $scope.tipo_de_fractura_2 = false; 
                        }
                        else if(inputValue == 8){
                            $scope.tipo_de_fractura_1 = false;
                            $scope.fractura_truama_octra = false;
                            $scope.tipo_de_fractura_2 = true; 
                        }
                        else{
                            $scope.tipo_de_fractura_1 = false;
                            $scope.fractura_truama_octra = false;
                            $scope.tipo_de_fractura_2 = false;
                        }
                    },100)
                }

                if(id == 'fractura_pathologica_por'){
                    setTimeout(function(){
                        var inputValue = $('[name="answer[fractura_pathologica_por]"]').val();
                        if(inputValue == 1){
                            $scope.tumor = true;
                            $scope.pathologica_otra = false;
                           }
                           else if(inputValue == 3){
                                $scope.pathologica_otra = true;
                                $scope.tumor = false;
                           }
                           else{
                                $scope.tumor = false;
                                $scope.pathologica_otra = false;
                           }
                    },100)
                }

                if(id == 'tipo_de_tumor'){
                    setTimeout(function(){
                        var inputValue = $('[name="answer[tipo_de_tumor]"]').val();
                        if(inputValue == 5){
                            $scope.tipo_de_tumor_otra = true;
                        }
                        else {
                            $scope.tipo_de_tumor_otra = false;
                        }
                    },100)
                }

                if(id == 'localization'){
                    setTimeout(function(){
                        var inputValue = $('[name="answer[localization]"]').val();
                        if(inputValue == 6){
                            $scope.localization_otra = true;
                        }
                        else {
                            $scope.localization_otra = false;
                        }
                    },100)
                }

                if(id == 'tipo_de_inflamacion'){
                    setTimeout(function(){
                        var inputValue = $('[name="answer[tipo_de_inflamacion]"]').val();
                        if(inputValue == 4){
                            $scope.inflamacion_otra = true;
                        }
                        else {
                            $scope.inflamacion_otra = false;
                        }
                    },100)
                }

                if(id == 'tipo_de_infeccion'){
                    setTimeout(function(){
                        var inputValue = $('[name="answer[tipo_de_infeccion]"]').val();
                        if(inputValue == 5){
                            $scope.tipo_de_infeccion_otra = true;
                        }
                        else {
                            $scope.tipo_de_infeccion_otra = false;
                        }
                    },100)
                }

                if(id == 'estructura_afectada'){
                    setTimeout(function(){
                        var inputValue = $('[name="answer[estructura_afectada]"]').val();
                        if(inputValue == 5){
                            $scope.estructura_afectada_otra = true;
                        }
                        else {
                            $scope.estructura_afectada_otra = false;
                        }
                    },100)
                }

                if(id == 'tipo_de_reintervencion'){
                    setTimeout(function(){
                        var inputValue = $('[name="answer[tipo_de_reintervencion]"]').val();
                        if(inputValue == 12){
                            $scope.tipo_de_reintervencion_otra = true;
                        }
                        else {
                            $scope.tipo_de_reintervencion_otra = false;
                        }
                    },100)
                }

                if(id == 'segun_segmento_escogido_en_el_inicio'){
                    setTimeout(function(){
                        var inputValue = $('[name="answer[segun_segmento_escogido_en_el_inicio]"]').val();
                    },100)
                }

                if(id == 'segmento'){
                    setTimeout(function(){
                        var inputValue = $('[name="answer[segmento]"]').val();
                        console.log(inputValue);
                        if(inputValue < 4){
                            $scope.cervical = true;
                        }
                        else {
                            $scope.cervical = false;
                        }
                        if(inputValue >4){
                            $scope.oswerty = true;
                        }
                        else{
                            $scope.oswerty = false;
                        }
                        if(inputValue == 4){
                            $scope.escala = true;
                        }
                        else{
                            $scope.escala = false;
                        }
                    },100)
                }

                if(id == 'escala'){
                    setTimeout(function(){
                        var inputValue = $('[name="answer[escala]"]').val();
                        console.log(inputValue);
                        if(inputValue == 1){
                            $scope.cervical = true;
                            $scope.oswerty = false;
                        }
                        else {
                            $scope.cervical = false;
                            $scope.oswerty = true;
                        }
                    },100)
                }
            }

            $scope.consoled = function(val){
                console.log(val)
            }
            // ----------------------------------------------------------------
            // Cirugia
            // ----------------------------------------------------------------

            $scope.getQuestionId = function (sectionId, property) {
                // console.log(sectionId);
                // console.log(property);
                return $('[name="answers[' + sectionId + '][' + property + ']"]').val();
            };

            $scope.getReemplazoId = function(sectionId,value){
                var tipo_1 = [{id: 1, name: "Cementado"},{id: 2, name: "No cementado"},{id: 3, name: "Híbrida"}];
                var tipo_2 = [{id: 1, name: "Cementado"}];
                if(sectionId ==='reemplazo' && $scope.response.sectionType === 2){
                    if(value === 2){
                        $scope.currentQuestion[2]['tipo'] = undefined;
                        $scope.response.sections[1].questions[9].answers = tipo_2;
                    }
                    else{
                        $scope.response.sections[1].questions[9].answers = tipo_1;
                    }
                    $scope.queueRefreshQuestions();
                    // console.log($scope.response.sections[1].questions[9].answers)  
                }
            }

            $scope.getMarcaId = function () {
                var reemplazoId = $('[name="answers[2][reemplazo]"]').val(),
                        tipoId = $('[name="answers[2][tipo]"]').val(),
                        $marca = $('[name="answers[2][marca_' + reemplazoId + '_' + tipoId + ']"]');
                return $marca.val();
            };
            $scope.getReemplazoVastago = function () {
                var reemplazoId = $('[name="answers[2][reemplazo]"]').val(),
                        tipoId = '10' + $('[name="answers[2][vastago]"]').val();
                return 'marca_' + reemplazoId + '_' + tipoId;
            };
            $scope.getReemplazoVastagoMarca = function () {
                var reemplazoId = $('[name="answers[2][reemplazo]"]').val(),
                        tipoId = '10' + $('[name="answers[2][vastago]"]').val(),
                        marcaId = $('[name="answers[2][' + $scope.getReemplazoVastago() + ']"]').val();
                return 'referencia_' + reemplazoId + '_' + tipoId + '_' + marcaId;
            };
            $scope.getReemplazoCopa = function () {
                var reemplazoId = $('[name="answers[2][reemplazo]"]').val(),
                        tipoId = '20' + $('[name="answers[2][copa]"]').val();
                return 'marca_' + reemplazoId + '_' + tipoId;
            };
            $scope.getReemplazoCopaMarca = function () {
                var reemplazoId = $('[name="answers[2][reemplazo]"]').val(),
                        tipoId = '20' + $('[name="answers[2][copa]"]').val(),
                        marcaId = $('[name="answers[2][' + $scope.getReemplazoCopa() + ']"]').val();
                return 'referencia_' + reemplazoId + '_' + tipoId + '_' + marcaId;
            };
            // ----------------------------------------------------------------
            // Cirugia - Artroscopia
            // ----------------------------------------------------------------

            $scope.txtProcedimiento = 'procedimiento';
            $scope.fabricantesList = [];
            $scope.refreshFabricantesList = function () {
                setTimeout(function () {
                    var fabricanteId = 'fabricante_' + $('[name="answers[3][procedimiento]"]').val();
                    $scope.fabricantesList = _.find($scope.artroscopia.fabricantes, {id: fabricanteId}).answers;
                    $scope.formQuestion[3]['fabricante'] = {};
                    $scope.formQuestion[3]['tecnica'] = {};
                    $scope.formQuestion[3]['implante'] = {};
                    $scope.formQuestion[3]['nombre_comercial'] = {};
                    $scope.tecnicasList = [];
                    $scope.implantesList = [];
                    $scope.nombresComercialesList = [];
                }, 100);
            };
            $scope.refreshSubsegmentListNew = function () {
                setTimeout(function () {
                    var region = 'sub_segment_' + $('[name="answers[1][region]"]').val();
                    $scope.subsegmentList = _.find($scope.cirugiaosteo.sub_segment, {id: region}).answers;
                    $scope.formQuestion[1]['tratamiento'] = {};
                    $scope.formQuestion[1]['fabricante'] = {};
                    $scope.formQuestion[1]['tecnica'] = {};
                    $scope.formQuestion[1]['implante'] = {};
                    $scope.formQuestion[1]['nombre_comercial'] = {};
                    $scope.formQuestion[1]['sub_segment'] = {};
                    //  $scope.formQuestion[1]['cups'] = {};
                    $scope.tecnicasList = [];
                    $scope.implantesList = [];
                    $scope.nombresComercialesList = [];
                }, 100);
            };
            $scope.refreshFabricantesListNew = function () {
                setTimeout(function () {

                    if($scope.alreadySelSub){
                      var tratamiento = 'fabricante_' + $scope.alreadySelSub.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '') + '_' + $('[name="answers[1][tratamiento]"]').val().toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');  
                        //console.log(tratamiento)
                    }
                    else{
                        var tratamiento = 'fabricante_' + $('[name="answers[1][sub_segment]"]').val() + '_' + $('[name="answers[1][tratamiento]"]').val().toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
                        //console.log(tratamiento)
                    }
                    
                    $scope.fabricantesList = _.find($scope.cirugiaosteo.fabricantes, {id: tratamiento}).answers;
                    $scope.formQuestion[1]['fabricante'] = {};
                    $scope.formQuestion[1]['tecnica'] = {};
                    $scope.formQuestion[1]['implante'] = {};
                    $scope.formQuestion[1]['nombre_comercial'] = {};
                    $scope.tecnicasList = [];
                    $scope.implantesList = [];
                    $scope.nombresComercialesList = [];
                }, 100);
            };
            $scope.subchosen = function () {
                $scope.chosen = true;
            }
            $scope.tecnicasList = [];
            $scope.refreshTecnicasList = function () {
                setTimeout(function () {

                    if($('[name="answers[3][fabricante]"]').val() != 'sugerir'){
                         var tecnicaId = 'tecnica_' +
                            $('[name="answers[3][procedimiento]"]').val() + '_' +
                            $('[name="answers[3][fabricante]"]').val();
                            $scope.tecnicasList = _.find($scope.artroscopia.tecnicas, {id: tecnicaId}).answers;
                            $scope.formQuestion[3]['tecnica'] = {};
                            $scope.formQuestion[3]['implante'] = {};
                            $scope.formQuestion[3]['nombre_comercial'] = {};
                            $scope.implantesList = [];
                            $scope.nombresComercialesList = [];
                    }
                    else{
                        $('[name="answers[3][tecnica]"]').hide();
                        $('[name="answers[3][implante]"]').hide();
                    }
                }, 100);
            };
            $scope.implantesList = [];
            $scope.refreshImplantesList = function () {
                setTimeout(function () {
                    var implanteId = 'implante_' +
                            $('[name="answers[3][procedimiento]"]').val() + '_' +
                            $('[name="answers[3][fabricante]"]').val() + '_' +
                            $('[name="answers[3][tecnica]"]').val();
                    $scope.implantesList = _.find($scope.artroscopia.implantes, {id: implanteId}).answers;
                    $scope.formQuestion[3]['implante'] = {};
                    $scope.formQuestion[3]['nombre_comercial'] = {};
                    $scope.nombresComercialesList = [];
                }, 100);
            };
            $scope.nombresComercialesList = [];
            $scope.refreshNombresComercialesList = function () {
                setTimeout(function () {
                    var nombreComercialId = 'nombre_comercial_' +
                            $('[name="answers[3][procedimiento]"]').val() + '_' +
                            $('[name="answers[3][fabricante]"]').val() + '_' +
                            $('[name="answers[3][tecnica]"]').val() + '_' +
                            $('[name="answers[3][implante]"]').val();
                    $scope.nombresComercialesList = _.find($scope.artroscopia.nombres_comerciales, {id: nombreComercialId}).answers;
                }, 100);
            };
            $scope.refreshNombresComercialesListNew = function () {
                var tratamiento = $('[name="answers[1][tratamiento]"]').val().toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
                setTimeout(function () {
                    if($('[name="answers[1][fabricante]"]').val() != 'sugerir'){

                        var nombreComercialId = 'nombre_comercial_' + $scope.alreadySelSub + '_' + tratamiento + '_' + $('[name="answers[1][fabricante]"]').val().toLowerCase();
                        // console.log(nombreComercialId);
                        $scope.nombresComercialesListNew = _.find($scope.cirugiaosteo.nombres_comerciales, {id: nombreComercialId}).answers;
                    }                

                }, 100);
            };
            $(document).on('click', '.item-checkbox input', function () {
                $scope.queueRefreshQuestions();
            });
            // ----------------------------------------------------------------
            // OA Foundation
            // ----------------------------------------------------------------
            $scope.firstSegments = [];
            $scope.humanPart = null;
            $scope.firstSegment = 0;
            $scope.secondSegment = 0;
            $scope.tipo_fractura_1 = null;
            $scope.tipo_fractura_2 = null;
            $scope.tipo_fractura_3 = null;
            $scope.tipo_fractura = null;
            $scope.alreadySelSub = "";
            $scope.setFirstSegment = function (id) {
                //console.log(id)
                $scope.firstSegment = id;
                $scope.secondSegment = 0;
                //console.log($scope.originalPageTitle)
                var w = document.getElementsByTagName('input');
                for (var i = 0; i < w.length; i++) {
                    if (w[i].type == 'checkbox') {
                        w[i].checked = false;
                    }
                }
                $scope.resetCups = true;
                $scope.refreshSubsegmentListNew();
                //$scope.setRegion(id,id1);
                $scope.refreshQuestions();
            };

            $scope.setRegion = function(id,id1){
                if(id == 1){
                    setTimeout(function(){
                        $scope.formQuestion[1][id1] = {};
                        var regionValue = _.find($scope.response.cirugia_osteo.region.answers,{id:'Fémur'});
                        $scope.formQuestion[1]['region'] = regionValue;
                        // console.log($scope.formQuestion[1][$scope.response.cirugia_osteo.region.id]);
                        // console.log($scope.response.cirugia_osteo.region);
                    },2000)
                    
                }
                if(id == 2){
                    console.log('Rotula');
                }
                if(id == 3){
                    console.log("Tibiya");
                }
            }

            $scope.setSecondSegment = function (id) {
                $scope.secondSegment = id;
            };
            $scope.haveSecondSegment = function () {
                var secondSegmentList = $scope.secondSegmentList();
                return typeof secondSegmentList[0] !== 'undefined' && typeof secondSegmentList[0].id !== 'undefined';
            };
            $scope.secondSegmentList = function () {
                var results = _.find($scope.firstSegments, {id: $scope.firstSegment});
                return typeof results !== 'undefined' ? results.items : [];
            };
            $scope.optionsList = function () {
                if ($scope.firstSegment > 0 && !$scope.haveSecondSegment()) {
                    return [_.find($scope.firstSegments, {id: $scope.firstSegment})];
                }

                if (Number($scope.secondSegment)) {
                    var results = _.find($scope.secondSegmentList(), {id: $scope.secondSegment});
                    if (typeof results.image !== 'undefined') {
                        return [results]
                    }

                    return results.items;
                }

                return [];
            };
            $scope.setOAFoundation = function (image) {
                //console.log($scope.firstSegment, $scope.secondSegment);


                
                if ($scope.originalPageTitle == 'Cadera / Cirugía')
                {
                    if ($scope.firstSegment == 1)
                    {
                        $scope.alreadySel = "Pelvis";
                        $scope.alreadySelSub = "pelvis";
                    }
                    if ($scope.firstSegment == 2)
                    {
                        $scope.alreadySel = "Fémur";
                        $scope.alreadySelSub = "femur-proximal";
                    }
                }

                if ($scope.originalPageTitle == 'Rodilla / Cirugía')
                {
                    if ($scope.firstSegment == 3)
                    {
                        $scope.alreadySel = "Tibia";
                        $scope.alreadySelSub = "tibia-proximal";
                    }
                    if ($scope.firstSegment == 2)
                    { 
                        $scope.alreadySel = "Rotula";
                        $scope.alreadySelSub = "Rotula";

                        $scope.response.cirugia_osteo.tratamiento.answers = $scope.rotulaTreatmento;
                    }
                    else{
                        $scope.response.cirugia_osteo.tratamiento.answers = $scope.defaultTreatmento;
                    }
                    if ($scope.firstSegment == 1)
                    {
                        $scope.alreadySel = "Fémur";
                        $scope.alreadySelSub = "femur-distal";
                    }
                }

                if ($scope.originalPageTitle == 'Hombro / Cirugía')
                {
                    if ($scope.firstSegment == 1)
                    {
                        $scope.alreadySel = "Calvicula";
                        if($scope.secondSegment == 1){
                            $scope.alreadySelSub = "clavicula-proximal";
                        }
                        if($scope.secondSegment == 2){
                            $scope.alreadySelSub = "clavicula-diafisiaria";
                        }
                        if($scope.secondSegment == 3){
                            $scope.alreadySelSub = "clavicula-distal";
                        }
                    }

                    if ($scope.firstSegment == 2)
                    {   
                        $scope.alreadySel = "Escápula";
                        $scope.alreadySelSub = "escapula";
                        $scope.response.cirugia_osteo.tratamiento.answers = $scope.escapulTreatmento;
                    }
                    else{
                        $scope.response.cirugia_osteo.tratamiento.answers = $scope.defaultTreatmento;
                    }

                    if ($scope.firstSegment == 3)
                    {
                        $scope.alreadySel = "Humero";
                        $scope.alreadySelSub = "humero-proximal";
                    }
                }
                $scope.tipo_fractura = $scope.getImageName(image);
                $scope.firstSegment = 0;
                $scope.secondSegment = 0;
                $scope.thirdSegment = 0;
                $scope.modal.hide();
            };
            $scope.getImageName = function (image) {
                return image.slice(0, -4);
            };

            $scope.openTipoModal = function(){
                 $scope.tipoModal.show();
                
            }

            $scope.dataUpdate = function(section){
                    if(section.indexOf("cirugia")>0){
                        $scope.response.cirugia_osteo.tratamiento.answers = $scope.defaultTreatmento; 
                        _.findKey($scope.response.cirugia_artroscopia.fabricantes,function(key){
                            if(key.answers!= undefined){
                                var data = {
                                    id : 'sugerir',
                                    name : "Sugerir Fabricante"
                                }
                                key.answers.push(data);
                            }
                        })
                        _.findKey($scope.response.cirugia_artroscopia.nombres_comerciales,function(key){
                            if(key.answers!= undefined){
                                var data = {
                                    id : 'referencia',
                                    name : "Sugerir Nombre Comercial"
                                }
                                key.answers.push(data);
                            }
                        })
                        _.findKey($scope.response.cirugia_osteo.fabricantes,function(key){
                            if(key.answers!= undefined){
                                var data = {
                                    id : 'sugerir',
                                    name : "Sugerir Fabricante"
                                }
                                key.answers.push(data);
                            }
                        })
                        _.findKey($scope.response.cirugia_osteo.nombres_comerciales,function(key){
                            if(key.answers!= undefined){
                                var data = {
                                    id : 'referencia',
                                    name : "Sugerir Nombre Comercial"
                                }
                                key.answers.push(data);
                            }
                        })
                        _.findKey($scope.response.cirugia_protesis.marcas,function(key){
                            if(key.answers!= undefined){
                                var data = {
                                    id : 'sugerir',
                                    name : "Sugerir Marca"
                                }
                                key.answers.push(data);
                            }
                        })
                        _.findKey($scope.response.cirugia_protesis.referencias,function(key){
                            if(key.answers!= undefined){
                                var data = {
                                    id : 'referencia',
                                    name : "Sugerir Referencia"
                                }
                                key.answers.push(data);
                            }
                        })

                            if(section.indexOf('cadera') !== -1){
                                _.findKey($scope.response.cirugia_protesis.first.marcas,function(key){
                                if(key.answers!= undefined){
                                    var data = {
                                        id : 'sugerir',
                                        name : "Sugerir Marca"
                                    }
                                    key.answers.push(data);
                                }
                            })
                            _.findKey($scope.response.cirugia_protesis.second.marcas,function(key){
                                if(key.answers!= undefined){
                                    var data = {
                                        id : 'sugerir',
                                        name : "Sugerir Marca"
                                    }
                                    key.answers.push(data);
                                }
                            })
                                _.findKey($scope.response.cirugia_protesis.first.referencias,function(key){
                                if(key.answers!= undefined){
                                    var data = {
                                        id : 'referencia',
                                        name : "Sugerir Referencia"
                                    }
                                    key.answers.push(data);
                                }
                            })
                            _.findKey($scope.response.cirugia_protesis.second.referencias,function(key){
                                if(key.answers!= undefined){
                                    var data = {
                                        id : 'referencia',
                                        name : "Sugerir Referencia"
                                    }
                                    key.answers.push(data);
                                }
                            })
                        }

                        var array_index = window.localStorage.getItem('procedimiento');
                        $scope.closeModal($scope.sections[array_index]);   
                    }
                }


                $scope.consoleValue = function(id,value){
                    if(!_.isObject(value)){
                        var data = {
                            id:id,
                            points:parseInt(value)
                        };
                        var index = _.findIndex($scope.points,{'id' : id});
                        if(index <=-1){
                            $scope.points.push(data);
                        }
                        else{
                            $scope.points[index] = data;
                        }

                    }
                    else{

                        var data = {
                            id:id,
                            points:parseInt(value.point)
                        };
                        var index = _.findIndex($scope.points,{'id' : id});
                        if(index <=-1){
                            $scope.points.push(data);
                        }
                        else{
                            $scope.points[index] = data;
                        }
                    }
                    $scope.totalConsultaPoints();
                }

                $scope.totalConsultaPoints = function(){
                    var totalPoint  = 0;
                    angular.forEach($scope.points, function(value, key) {
                        totalPoint = totalPoint+value.points;
                    })
                    var status = ScoreResult.findResult($scope.pageTitle,totalPoint);
                    
                    return status;
                }
        });
