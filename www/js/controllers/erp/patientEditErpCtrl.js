angular.module('app.controllers')

        .controller('patientEditErpCtrl', function ($scope, $timeout, offlineWebservice,$localStorage,offlinedata, $state, $rootScope, $stateParams, $http, $location, $filter, FlashMessage, Auth, ionicDatePicker) {
            $scope.token = Auth.token();
            $scope.apiData = {}; 
            $scope.octra = false;
            
            $scope.showLoading();
            $http.get($rootScope.endPoint + 'erp/patient/create?token=' + Auth.token(), $scope.patient).success(function (response) {
                $scope.lists = response;
                $localStorage.permanentData = response;
                console.log($stateParams.patientId);
                if ($stateParams.patientId > 0) {

                    $http.get($rootScope.endPoint + 'erp/patient/' + $stateParams.patientId + '?token=' + Auth.token()).
                            success(function (response) {
                                $scope.lists.personalHistories;
                                $scope.hideLoading();
                                $scope.patient = response;
                                offlinedata.editProfileData(response);
                                $scope.updateDatas(response);
                               
                                               
                            }).
                            error(function (error, errorCode) {
                                $scope.hideLoading();
                                if (errorCode == 401) {
                                    Auth.logout();
                                } else {
                                    var profileData1 = offlinedata.getEditProfileData($stateParams.patientId);
                                    if(!_.isEmpty(profileData1)){
                                        $scope.patient = profileData1;
                                        $scope.updateDatas(profileData1);

                                    }
                                    else{
                                        FlashMessage.showError('Se ha producido un error, intente de nuevo que no tiene ningún dato anterior.'); 
                                        $state.go('tab_erp.patient_search');
                                    }  
                                }
                            });
                } else {
                    $state.go('tab_erp.patient_search');
                }
            }).error(function (error, errorCode) {
                 $scope.hideLoading();
                    if(!_.isEmpty($localStorage.permanentData)){

                        $scope.lists = $localStorage.permanentData;
                        console.log($scope.lists);
                        var profileData1 = offlinedata.getEditProfileData($stateParams.patientId);
                        if(!_.isEmpty(profileData1)){
                            $scope.patient = profileData1;
                            $scope.updateDatas(profileData1);
                        }
                        else{
                            FlashMessage.showError('Se ha producido un error, intente de nuevo que no tiene ningún dato anterior.'); 
                            $state.go('tab_erp.patient_search');
                        } 
                    }
                    else{
                        FlashMessage.showError('Se ha producido un error, intente de nuevo que no tiene ningún dato anterior.'); 
                        $state.go('tab_erp.patient_search');
                    }    
            });


            $scope.showOctra = false;
            $scope.lists = {
                'cities': [],
                'personalHistories': [],
                'genders': []
            };
            $scope.showOthers = function ()
            {
                var menu = $scope.patient.personal_history_id.name;
                if (menu == 'Otro. Cuál?')
                {
                    $scope.showOctra = true;
                } else
                {
                    $scope.showOctra = false;
                }
            }

            $scope.onCitySelect = function(data){
                $scope.institution = [];
                console.log(data)
               $scope.institution =  _.filter($scope.lists.institution, function(o) { return o.city_id == data.id; });

            }

            $scope.saveUpdate = function (form)
            {
                console.log(form.$valid);
                if(form.$valid){

                    $scope.showLoading();
                    $http.post($rootScope.endPoint + 'erp/patient/create?token=' + Auth.token(), $scope.patient, $stateParams).success(function (response) {
                        $scope.hideLoading();
                        if (response.success) {
                            FlashMessage.showInformation('El paciente ha sido modificado', function () {
                                console.log(response);
                                $location.path('/tab_erp/patient/' + response.patientid);
                            });
                        } else {
                            // FlashMessage.showGeneralError();
                            $state.go('tab_erp.patient_search');
                        }
                    }).error(function (error, errorCode) {
                        if (errorCode == 401) {
                            $scope.hideLoading();
                            Auth.logout();
                        } else {
                            $timeout(function () {
                                $scope.hideLoading();
                                 $scope.apiData.api = $rootScope.endPoint + 'erp/patient/create?token='  + Auth.token();
                                 $scope.apiData.data = $scope.patient;
                                 $scope.apiData.type = 'patient_edit';
                                 offlineWebservice.storeApiRequest($scope.apiData);
                                 FlashMessage.showInformation('Usted está fuera de línea, la información se sincronizará cuando haya conexión de datos');
                                 $state.go('tab_erp.patient_search');
                            }, 1000);
                        }
                    });   
                }
                else{
                    console.log('form error')
                }
            }


            $scope.save = function () {
                $scope.showLoading();
                $http.post($rootScope.endPoint + 'erp/patient/create?token=' + Auth.token(), $scope.patient).success(function (response) {
                    $scope.hideLoading();
                    if (response.success) {
                        FlashMessage.showInformation('El paciente ha sido creado', function () {
                            $location.path('/tab_erp/patient/' + response.patient.id);
                        });
                    }
                    else{
                        $state.go('tab_erp.patient_search');
                    }
                }).error(function (error, errorCode) {
                    $scope.hideLoading();
                    if (errorCode == 401) {
                        Auth.logout();
                    } else {
                        FlashMessage.manageError(error, errorCode);
                    }
                });
            };

            var inputDateParams = {
                callback: function (val) {  //Mandatory
                    var date = new Date(val),
                            day = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()),
                            month = (date.getMonth() + 1);

                    month = (month < 10 ? '0' + month : month);

                    $scope.patient.birth_date = date.getFullYear() + '-' + month + '-' + day;
                    $scope.birth_date = new Date(val);
                },
                setLabel: 'Seleccionar',
                closeOnSelect: false,
                from: new Date(1920, 1, 1), //Optional
                to: new Date(2016, 10, 30) //Optional
            };
            $scope.locadata = function ($query) {
                console.log(1);
                var countries = $scope.lists.personalHistories;
                console.log(countries);
                return countries.filter(function (country) {
                    console.log(country.name.toLowerCase());
                    return true
                    return country.name.toLowerCase().indexOf($query.toLowerCase()) != -1;
                });
            };
            $scope.openDatePicker = function () {
                inputDateParams.inputDate = $scope.birth_date;
                ionicDatePicker.openDatePicker(inputDateParams);
            };

            $scope.updateDatas = function(response){
                $scope.patient.number_id = parseInt(response.number_id);
                $scope.patient.phone_mobile = parseInt(response.phone_mobile);
                $scope.patient.contact_phone = parseInt(response.contact_phone);
                $scope.patient.weight = parseInt(response.weight);
                console.log(response);
                $scope.birth_date = new Date($scope.patient.birth_date);
                var lengthCity = $scope.lists.cities.length;
                var personal_history_length = $scope.lists.personalHistories.length;
                var genderLength = $scope.lists.genders.length;
                var institutionLength = $scope.lists.institution.length;
                var personale = [];
                angular.forEach($scope.patient.personal_history_id,function(value,key){
                    if(value != ','){
                        var index = _.findIndex($scope.lists.personalHistories,{'id':parseInt(value)});
                        personale.push($scope.lists.personalHistories[index]);
                    }
                })
                $scope.setPersonales(personale);
                //var length
                console.log($scope.lists.personalHistories);
                console.log($scope.patient.personal_history_id);
                for ($i = 0; $i < lengthCity; $i++)
                {
                    var subArray = $scope.lists.cities[$i];
                    if (subArray.id == $scope.patient.city_id)
                    {
                        $scope.onCitySelect(subArray);
                        $scope.patient.city_id = subArray;

                    }
                }
                // for ($i = 0; $i < personal_history_length; $i++)
                // {
                //     var subArray = $scope.lists.personalHistories[$i];
                //     if (subArray.id == $scope.patient.personal_history_id)
                //     {
                //         $scope.patient.personal_history_id = subArray;

                //     }
                // }
                for ($i = 0; $i < genderLength; $i++)
                {
                    var subArray = $scope.lists.genders[$i];
                    if (subArray.name == $scope.patient.gender)
                    {
                        $scope.patient.gender = subArray;

                    }
                }

                    if(angular.isObject($scope.patient.clinical_institution)){
                        for ($i = 0; $i < institutionLength; $i++)
                        {
                            var subArray = $scope.lists.institution[$i];
                               if (subArray.name == $scope.patient.clinical_institution)
                                {
                                    $scope.patient.clinical_institution = subArray;

                                } 
                            
                        }
                     }
                     else{      
                                var name = $scope.patient.clinical_institution;
                                $scope.patient.clinical_institution = {};
                                $scope.patient.clinical_institution.name = name;
                            }
            }



            $scope.setPersonales = function(options){
                console.log(options);
                $scope.patient.personal_history_id = {};
                $scope.patient.personal_history_id.id = '';
                $scope.patient.personal_history_id.name = '';
                var i= 0,flag = 0;
                angular.forEach(options,function(value,key){
                    i++;
                    if(i == options.length){
                        console.log(value);
                        $scope.patient.personal_history_id.id += value.id;    
                    }
                    else{
                        $scope.patient.personal_history_id.id += value.id+','
                    }
                    if(i == options.length){
                        $scope.patient.personal_history_id.name += value.name;    
                    }
                    else{
                        $scope.patient.personal_history_id.name += value.name+','
                    }
                    if(value.id == 5){
                        flag++;
                    }
                })
                if(flag >0){
                    $scope.octra = true;
                }
                else{
                    $scope.octra = false;
                }
            }
        });
