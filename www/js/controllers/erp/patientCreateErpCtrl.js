angular.module('app.controllers')

        .controller('patientCreateErpCtrl', function ($scope, $timeout, $state,offlineWebservice, $localStorage,$rootScope, $http, $location, $filter, FlashMessage, Auth, ionicDatePicker) {

            $scope.patient = {
                number_id: null,
                first_name: null,
                last_name: null,
                phone_mobile: null,
                address: null,
                city_id: null,
                email: null,
                gender: null,
                birth_date: null,
                height: null,
                weight: null, 
                diagnostic: null,
                personal_history: null,
                personal_history_id: null,
                doctor_id: null,
                personal_history_other: null,
                clinical_institution: null,
                contact_name: null,
                contact_phone: null,
                personal_history_data: null
            };
            $scope.birth_date = new Date(1980, 0, 1);
            $scope.showOctra = false;
            $scope.apiData = {};
            $scope.lists = {
                'cities': [],
                'personalHistories': [],
                'genders': []
            };
            $scope.showOthers = function ()
            {
                var menu = $scope.patient.personal_history_id.name;
                if (menu == 'Otro. Cuál?')
                {
                    $scope.showOctra = true;
                } else
                {
                    $scope.showOctra = false;
                }
            }
            $http.get($rootScope.endPoint + 'erp/patient/create?token=' + Auth.token(), $scope.patient).success(function (response) {
                $scope.lists = response;
                $localStorage.permanentData = response;

            }).error(function (error, errorCode) {
                if(!_.isEmpty($localStorage.permanentData)){
                    $scope.lists = $localStorage.permanentData;
                }
                else{
                    FlashMessage.manageError(error, errorCode);
                    $location.path('/tab_erp/patient_search');
                }
            });

            $scope.onCitySelect = function(data){
                $scope.institution = [];
                console.log(data)
               $scope.institution =  _.filter($scope.lists.institution, function(o) { return o.city_id == data.id; });

            }

            $scope.save = function (form) {
                if(form.$valid){
                     $scope.showLoading();
                    $http.post($rootScope.endPoint + 'erp/patient/create?token=' + Auth.token(), $scope.patient).success(function (response) {
                        $scope.hideLoading();
                        if (response.success) {
                            FlashMessage.showInformation('El paciente ha sido creado', function () {
                                $location.path('/tab_erp/patient/' + response.patient.id);
                            });
                        } else {
                            FlashMessage.showGeneralError();
                        }
                    }).error(function (error, errorCode) {
                        $timeout(function () {
                            $scope.hideLoading();
                            if (errorCode == 401) {
                                Auth.logout();
                            } else {
                                if(errorCode == 422){
                                    FlashMessage.showError('Todos los campos requeridos! Completa todo el campo');
                                }
                                else{
                                     console.log(errorCode);
                                     $scope.apiData.api = $rootScope.endPoint + 'erp/patient/create?token='  + Auth.token();
                                     $scope.apiData.data = $scope.patient;
                                     $scope.apiData.type = 'patient_creation';
                                     offlineWebservice.storeApiRequest($scope.apiData);
                                     FlashMessage.showInformation('Esta fuera de línea, los datos se actualizarán con una conexión a internet');
                                     $state.go('tab_erp.patient_search');   
                                }
                            }
                        }, 1000);
                    });
                }
                else{
                    console.log('form error')
                }
            };

            var inputDateParams = {
                callback: function (val) {  //Mandatory
                    var date = new Date(val),
                            day = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()),
                            month = (date.getMonth() + 1);

                    month = (month < 10 ? '0' + month : month);

                    $scope.patient.birth_date = date.getFullYear() + '-' + month + '-' + day;
                    $scope.birth_date = new Date(val);
                },
                setLabel: 'Seleccionar',
                closeOnSelect: false,
                from: new Date(1920, 01, 01), //Optional
                to: new Date(2016, 10, 30) //Optional
            };
            $scope.locadata = function ($query) {
                console.log(1);
                    var countries = $scope.lists.personalHistories;
                    console.log(countries);
                  return countries.filter(function (country) {
                       console.log(country.name.toLowerCase());
                       return true
                       return country.name.toLowerCase().indexOf($query.toLowerCase()) != -1;
                  });
            };
            $scope.openDatePicker = function () {
                inputDateParams.inputDate = $scope.birth_date;
                ionicDatePicker.openDatePicker(inputDateParams);
            };

            $scope.setPersonales = function(options){
                $scope.patient.personal_history_id = {};
                $scope.patient.personal_history_id.id = '';
                $scope.patient.personal_history_id.name = '';
                var i= 0,flag = 0;
                angular.forEach(options,function(value,key){
                    i++;
                    if(i == options.length){
                        $scope.patient.personal_history_id.id += value.id;    
                    }
                    else{
                        $scope.patient.personal_history_id.id += value.id+','
                    }
                    if(i == options.length){
                        $scope.patient.personal_history_id.name += value.name;    
                    }
                    else{
                        $scope.patient.personal_history_id.name += value.name+','
                    }
                    if(value.id == 5){
                        flag++;
                    }
                })
                if(flag >0){
                    $scope.octra = true;
                }
                else{
                    $scope.octra = false;
                }
            }
        });
