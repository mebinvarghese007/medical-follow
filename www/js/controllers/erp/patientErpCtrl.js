angular.module('app.controllers')

        .controller('patientErpCtrl', function ($scope, $timeout, $interval,$ionicBody,$localStorage,offlinedata,$ionicModal,$state,$location, $rootScope, $stateParams, $http, FlashMessage, $ionicActionSheet, Auth) {
            $scope.lists = {
                'cities': [],
                'personalHistories': [],
                'genders': []
            };
            $scope.octra = false;
            $scope.token = Auth.token();
            //remove model open class from body when page is loaded
              $ionicBody.removeClass('modal-open');

            if ($stateParams.patientId > 0) {
                $scope.showLoading();
                $http.get($rootScope.endPoint + 'erp/patient/create?token=' + Auth.token(), $scope.patient)
                .success(function (response) {
                    $scope.lists = response;
                    $localStorage.permanentData = response;
                    $http.get($rootScope.endPoint + 'erp/patient/' + $stateParams.patientId + '?token=' + Auth.token())
                    .success(function (response) {
                                $scope.hideLoading();
                                $scope.patient = response;
                                offlinedata.profileData(response);
                                var personale = [];
                                angular.forEach($scope.patient.personal_history_id,function(value,key){
                                    if(value != ','){
                                        var index = _.findIndex($scope.lists.personalHistories,{'id':parseInt(value)});
                                        personale.push($scope.lists.personalHistories[index]);
                                    }
                                })
                                $scope.setPersonales(personale);
                                var lengthCity = $scope.lists.cities.length;
                                for ($i = 0; $i < lengthCity; $i++)
                                {
                                    var subArray = $scope.lists.cities[$i];
                                    if (subArray.id == $scope.patient.city_id)
                                    {
                                        $scope.patient.city_id = subArray;

                                    }
                                }
                            }).
                            error(function (error, errorCode) {
                                $scope.hideLoading();
                                if (errorCode == 401) {
                                    Auth.logout();
                                } else {
                                    var profileData1 = offlinedata.getProfileData($stateParams.patientId);
                                    if(!_.isEmpty(profileData1)){
                                        $scope.patient = profileData1;
                                    }
                                    else{
                                        FlashMessage.showError('Se ha producido un error, intente de nuevo que no tiene ningún dato anterior.'); 
                                        $state.go('tab_erp.patient_search');
                                    }                                }
                            });
                }).
                error(function(error,errorCode){
                    if (errorCode == 401) {
                        $scope.hideLoading();
                        Auth.logout();
                    } else {
                        if(!_.isEmpty($localStorage.permanentData)){
                            $scope.lists = $localStorage.permanentData;
                            var profileData1 = offlinedata.getProfileData($stateParams.patientId);
                            if(!_.isEmpty(profileData1)){
                                 $timeout(function () {
                                    $scope.hideLoading();
                                }, 1000); 
                                $scope.patient = profileData1;
                            }
                            else{
                                $scope.hideLoading();
                                FlashMessage.showError('Se ha producido un error, intente de nuevo que no tiene ningún dato anterior.'); 
                                $state.go('tab_erp.patient_search');
                            } 
                        }
                        else{
                            $scope.hideLoading();
                            FlashMessage.showError('Se ha producido un error, intente de nuevo que no tiene ningún dato anterior.'); 
                            $state.go('tab_erp.patient_search');
                        } 
                        // FlashMessage.showError('Ha ocurrido un error, intente nuevamente');
                        // $state.go('tab_erp.patient_search');
                    }
                });
            } else {
                $state.go('tab_erp.patient_search');
            }
            // resolve modal issue
            // var notificationInterval;
            // notificationInterval = $interval(function() {
            //   $ionicBody.removeClass('modal-open');
            //   //  console.log("removing those body classes while the notification is up.");
            // }, 1000)
            $scope.goEdit = function () {
                $location.path('/tab_erp/patientEdit/' + $stateParams.patientId);
            }

                var sect1 = {id: 1, name: "Osteosíntesis"};
                var sect2 = {id: 2, name: "Prótesis"};
                var sect3 = {id: 3, name: "Artroscopia"};
                var sections = [sect1, sect2, sect3];
                $scope.sections = sections;
                
            //modal shown for selecting tipo de procedimiento **xtapps developer
            $ionicModal.fromTemplateUrl('templates/erp/form/tipo_de_procedimiento.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
             }).then(function (modal) {
                $scope.tipoModal = modal;
            });

             $scope.openModal = function () {
                $scope.tipoModal.show();
            };
            $scope.closeTipoModal = function(val){
                 window.localStorage.setItem('procedimiento', val-1);
                //console.log(window.localStorage.getItem('procedimiento'))
                $scope.tipoModal.hide();
                if($scope.actionSheetFrom === "Hombro"){
                    $state.go("tab_erp.form_hombro_cirugia", {"patientId": $stateParams.patientId});
                }
                if($scope.actionSheetFrom === "Cadera"){
                    $state.go("tab_erp.form_cadera_cirugia", {"patientId": $stateParams.patientId});
                }
                if($scope.actionSheetFrom === "Rodilla"){
                    $state.go("tab_erp.form_rodilla_cirugia", {"patientId": $stateParams.patientId});
                }
                if($scope.actionSheetFrom === "Columna"){
                    $state.go("tab_erp.form_columna_cirugia", {"patientId": $stateParams.patientId});
                }
            }
            $scope.showActionsheet = function (patientId) {
                $scope.patient_id = patientId;
                $ionicActionSheet.show({
                    titleText: 'Hombro',
                    buttons: [
                        {text: '<i class="icon ion-medkit"></i>	 Consulta'},
                        {text: '<i class="icon"><img  src="img/surgery_doctor-512.png" width=21px;></img></i>Cirugía'},
                        {text: '<i class="icon ion-android-clipboard"></i> Control'},
                    ],
                    cancelText: '<i class="icon ion-android-cancel"></i> Cancel',
                    cancel: function () {
                        console.log('CANCELLED');
                    },
                    buttonClicked: function (index) {
                        if (index == 0)
                        {
                            $state.go("tab_erp.form_hombro_consulta", {"patientId": patientId});
                        }
                        if (index == 1)
                        {
                            $scope.actionSheetFrom = "Hombro";
                            $scope.openModal();
                            //$state.go("tab_erp.form_hombro_cirugia", {"patientId": patientId});
                        }
                        if (index == 2)
                        {
                            $state.go("tab_erp.form_hombro_control", {"patientId": patientId});
                        }
                        return true;
                    },
                    destructiveButtonClicked: function () {
                        return true;
                    }
                });
            };
            $scope.showActionsheetCadera = function (patientId) {
                $scope.patient_id = patientId;
                $ionicActionSheet.show({
                    titleText: 'Cadera',
                    buttons: [
                        {text: '<i class="icon ion-medkit"></i>	 Consulta'},
                        {text: '<i class="icon"><img  src="img/surgery_doctor-512.png" width=21px;></img></i>Cirugía'},
                        {text: '<i class="icon ion-android-clipboard"></i> Control'},
                    ],
                    cancelText: '<i class="icon ion-android-cancel"></i> Cancel',
                    cancel: function () {
                    },
                    buttonClicked: function (index) {
                        if (index == 0)
                        {
                            $state.go("tab_erp.form_cadera_consulta", {"patientId": patientId});
                        }
                        if (index == 1)
                        {
                            $scope.actionSheetFrom = "Cadera";
                            $scope.openModal();
                            //$state.go("tab_erp.form_cadera_cirugia", {"patientId": patientId});
                        }
                        if (index == 2)
                        {
                            $state.go("tab_erp.form_cadera_control", {"patientId": patientId});
                        }
                        return true;
                    },
                    destructiveButtonClicked: function () {
                        return true;
                    }
                });
            };
            $scope.showActionsheetRodilla = function (patientId) {
                $scope.patient_id = patientId;
                $ionicActionSheet.show({
                    titleText: 'Rodilla',
                    buttons: [
                        {text: '<i class="icon ion-medkit"></i>  Consulta'},
                        {text: '<i class="icon"><img  src="img/surgery_doctor-512.png" width=21px;></img></i>Cirugía'},
                        {text: '<i class="icon ion-android-clipboard"></i> Control'},
                    ],
                    cancelText: '<i class="icon ion-android-cancel"></i> Cancel',
                    cancel: function () {
                    },
                    buttonClicked: function (index) {
                        if (index == 0)
                        {
                            $state.go("tab_erp.form_rodilla_consulta", {"patientId": patientId});
                        }
                        if (index == 1)
                        {
                            $scope.actionSheetFrom = "Rodilla";
                            $scope.openModal();
                            //$state.go("tab_erp.form_rodilla_cirugia", {"patientId": patientId});
                        }
                        if (index == 2)
                        {
                            $state.go("tab_erp.form_rodilla_control", {"patientId": patientId});
                        }
                        return true;
                    },
                    destructiveButtonClicked: function () {
                        return true;
                    }
                });
            };
            $scope.showActionsheetColumna = function (patientId) {
                $scope.patient_id = patientId;
                $ionicActionSheet.show({
                    titleText: 'Columna',
                    buttons: [
                        {text: '<i class="icon ion-medkit"></i>	 Consulta'},
                        {text: '<i class="icon"><img  src="img/surgery_doctor-512.png" width=21px;></img></i>Cirugía'},
                        {text: '<i class="icon ion-android-clipboard"></i> Control'},
                    ],
                    cancelText: '<i class="icon ion-android-cancel"></i> Cancel',
                    cancel: function () {
                    },
                    buttonClicked: function (index) {
                        if (index == 0)
                        {
                            $state.go("tab_erp.form_columna_consulta", {"patientId": patientId});
                        }
                        if (index == 1)
                        {
                            $scope.actionSheetFrom = "Columna";
                            $scope.openModal();
                            //$state.go("tab_erp.form_rodilla_cirugia", {"patientId": patientId});
                        }
                        if (index == 2)
                        {
                            $state.go("tab_erp.form_columna_control", {"patientId": patientId});
                        }
                        return true;
                    },
                    destructiveButtonClicked: function () {
                        return true;
                    }
                });
            };

            $scope.setPersonales = function(options){
                console.log(options);
                $scope.patient.personal_history = {};
                $scope.patient.personal_history_id.id = '';
                $scope.patient.personal_history.name = '';
                var i= 0,flag = 0;
                angular.forEach(options,function(value,key){
                    i++;
                    if(value.id != 5){
                        if(i == options.length){
                            $scope.patient.personal_history.name += value.name;    
                        }
                        else{
                            $scope.patient.personal_history.name += value.name+','
                        }   
                    }
                    else{
                        flag++;
                    }
                })
                if(flag >0){
                    if($scope.patient.personal_history_other && $scope.patient.personal_history_other != null){
                       $scope.patient.personal_history.name += $scope.patient.personal_history_other; 
                    }      
                }
                else{
                    $scope.octra = false;
                }
                console.log($scope.patient.personal_history)
            }

        }); 
