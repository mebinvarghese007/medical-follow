angular.module('app.controllers')

.controller('patientsErpCtrl', function($scope, $state, $rootScope, $stateParams, Auth) {
    $scope.patients = $stateParams.patients;

    if ($scope.patients == null) {
        $state.go('tab_erp.patient_search');
    }

    $scope.getPatient = function(id) {
        $state.go('tab_erp.patient', {patientId: id});
    };

    // Auth.checkLoggedOnline();
})
.controller('historyPatientsErpCtrl', function($scope, $state, $rootScope, $stateParams, Auth) {
    $scope.patients = $stateParams.patients;

    if ($scope.patients == null) {
        $state.go('tab_erp.patient_history_search');
    }

    $scope.getPatient = function(id) {
        console.log('here')
        $state.go('tab_erp.history', {id: id});
    };

    // Auth.checkLoggedOnline();
});
