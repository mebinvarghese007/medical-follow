angular.module('app.services', [])

.factory('BlankFactory', [function(){

}])

.service('BlankService', [function(){

}])

.service('webService', function($http,$q){
    return {
        invokeService: function(url,data) {         
            var deferred = $q.defer();
            var promise = deferred.promise;
            $http.post(url,data)
            .success(function(data) {       
                deferred.resolve(data);
            })
            .error(function(error,errorCode) {
                deferred.reject(errorCode);
            });
            
            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }        
            return promise;
        }
    } 
})

.factory('Patients', [function(){

}])

.factory('FlashMessage', function ($ionicPopup, $rootScope, $location) {
    var alertPopup;

    return {
        show: function (title, message, callback) {
            $rootScope.hideLoading();
            alertPopup = $ionicPopup.alert({
                title: title,
                template: message
            });
            alertPopup.then(function() {
                if (callback !== undefined) {
                    callback();
                }
            });
        },
        showEscala: function (title,message, callback) {
            $rootScope.hideLoading();
            alertPopup = $ionicPopup.alert({
                title: title,
                template: message,
                 buttons: [
                    {
                        text: '<b>Ok</b>',
                        type: 'button-balanced',
                    }
                 ]
            });
            alertPopup.then(function() {
                if (callback !== undefined) {
                    callback();
                }
            });
        },
        close: function() {
            if (alertPopup) {
                alertPopup.close();
            }
        },
        showError: function (message, callback) {
            this.show('Error', message, callback);
        },
        showGeneralError: function (callback) {
            this.show('Error', 'Ha ocurrido un error, por favor intente nuevamente', callback);
        },
        showInformation: function (message, callback) {
            this.show('Información', message, callback);
        },
        manageError: function(error, errorCode) {
            if (Number(errorCode) === 422) {
                var html = '';
                Object.keys(error).forEach(function(key) {
                    html += error[key] + "<br>";
                });
                this.show('Error', html);
            } else {
                this.show('Error', 'Ha ocurrido un error, por favor intente nuevamente');
            }
        }
    };
})

.factory("Session", function(){
    return {
        get : function(key) {
            return window.localStorage.getItem(key);
        },
        set : function(key, val) {
            return window.localStorage.setItem(key, val);
        },
        unset : function(key) {
            return window.localStorage.removeItem(key);
        }
    };
})

.factory("ScoreResult", function(){
    return {
        findResult : function(section,score){
            console.log('here');
            var patientScore = {};
             patientScore.status = 'No Found';
            if (section.toLowerCase().indexOf("hombro") >= 0) {
                if(score>30){
                    patientScore.status = score +' = Excelente';
                    patientScore.score = score;
                    patientScore.statusValue = 'Excelente';
                }
                if(score > 20 && score <=30){
                    patientScore.status = score +' = Bueno';  
                    patientScore.score = score;
                    patientScore.statusValue = 'Bueno';
                }
                if(score<11){
                    patientScore.status = score +' =Insuficiente';
                    patientScore.score = score;
                    patientScore.statusValue ='Insuficiente';
                }
                if(score >= 11 && score <=20){
                    patientScore.status = score +' = Satisfactorio';  
                    patientScore.score = score;
                    patientScore.statusValue = 'Satisfactorio';
                }
            }
            if (section.toLowerCase().indexOf("cadera") >= 0) {
                if(score<70){
                    patientScore.status = score +' = Insuficiente';
                    patientScore.score = score;
                    patientScore.statusValue = 'Insuficiente';
                }
                if(score >= 70 && score <=79){
                    patientScore.status = score +' = Satisfactorio'; 
                    patientScore.score = score;
                    patientScore.statusValue =  'Satisfactorio';
                }
                if(score>=80 && score <=89){
                    patientScore.status = score +' = Bueno';
                    patientScore.score = score;
                    patientScore.statusValue = 'Bueno';
                }
                if(score >= 90){
                    patientScore.status = score +' = Excelente'; 
                    patientScore.score = score;
                    patientScore.statusValue =  'Excelente';
                }
            }
            if (section.toLowerCase().indexOf("rodilla") >= 0) {
                if(score<60){
                    patientScore.status = score + ' = Insuficiente';
                    patientScore.score = score;
                    patientScore.statusValue = 'Insuficiente';
                }
                if(score >= 60 && score <=69){
                    patientScore.status = score + ' = Satisfactorio';  
                    patientScore.score = score;
                    patientScore.statusValue = 'Satisfactorio';
                }
                if(score>=70 && score <=79){
                    patientScore.status = score + ' = Bueno';
                    patientScore.score = score;
                    patientScore.statusValue = 'Bueno';
                }
                if(score >= 80){
                    patientScore.status = score + ' = Excelente';
                    patientScore.score = score;
                    patientScore.statusValue =  'Excelente'; 
                }
            }
            if (section.toLowerCase().indexOf("columna") >= 0) {
                var perScore = (score/50)*100;
                if(perScore>=0 && perScore<=20){
                    patientScore.status = parseInt(perScore) +'% = Discapacidad mínima';
                    patientScore.score = parseInt(perScore);
                    patientScore.statusValue = 'Discapacidad mínima';
                }
                if(perScore > 20 && perScore <=40){
                    patientScore.status = parseInt(perScore) +'% = Discapacidad moderada'; 
                    patientScore.score = parseInt(perScore);  
                    patientScore.statusValue = 'Discapacidad moderada';
                }
                if(perScore>40 && perScore <=60){
                    patientScore.status = parseInt(perScore) +'% = Discapacidad severa';
                    patientScore.score = parseInt(perScore);
                    patientScore.statusValue = 'Discapacidad severa';
                }
                if(perScore>60 && perScore <=80){
                    patientScore.status = parseInt(perScore) +'% = Discapacidad extremadamente severa';
                    patientScore.score = parseInt(perScore);
                    patientScore.statusValue = 'Discapacidad extremadamente severa';
                }
                if(perScore > 80){
                    patientScore.status = parseInt(perScore) +'% = Confinado en cama o síntomas exacerbados';
                    patientScore.score = parseInt(perScore);
                    patientScore.statusValue =   'Confinado en cama o síntomas exacerbados';
                }
            }

            return patientScore
        }
    };
})

.factory('Auth', function($http, $q, $rootScope, Session, $location){
    return {
        logout: function(){
            var token = Session.get('promed-token');
            Session.unset('promed-token');
            $http.get($rootScope.endPoint + 'auth/logout?token=' + token);
            window.localStorage.clear();
            $location.path("/login");
        },
        isLogged: function(){
            return (Session.get('promed-token') !== '' && Session.get('promed-token') !== 'null' && Session.get('promed-token') !== null);
        },
        checkLogged: function(){
            if (! this.isLogged() && $location.path() !== "/login") {
                $location.path("/login");
            }

            if ($location.path() === '/login' && this.isLogged()) {
                if (Session.get('promed-erp') == 'true') {
                    $location.path("/tab_erp/home");
                } else {
                    $location.path("/tab/home");
                }
            }
        },
        checkLoggedOnline: function(callback) {
            var self = this;
            $http.get($rootScope.endPoint + 'auth/check?token=' + this.token()).
            success(function(){
                if (typeof callback != 'undefined') callback(true);
            }).
            error(function(error, errorCode){
                if (typeof callback != 'undefined') {
                    callback(true);
                } else if (errorCode == 401) {
                    self.logout();
                }
            });
        },
        token: function(){
            return Session.get('promed-token');
        }
    };
})

.factory('Section', function($http, $q, $rootScope, Session, $location){
    return {

    };
})


.factory('offlinedata', function($http, $q, $parse, $rootScope, Session, $location,$localStorage){
    return {

        storeSearchResult (data){
            //console.log(_.isArray(data));
         angular.forEach(data, function(value, key) {
                if(_.isArray($localStorage.searchResultList)){
                       var index =  _.findIndex($localStorage.searchResultList, {'id':value.id});
                       // console.log(index);
                       if(index==-1){
                        $localStorage.searchResultList.push(value);
                       }
                       else{
                        $localStorage.searchResultList[index] = value;
                       }
                }
                else{
                    $localStorage.searchResultList = [];
                    $localStorage.searchResultList.push(value);
                }
        });
        },


        searchResult (data){
            function removeAccents(value){
                return value
                            .replace(/á/g, 'a')            
                            .replace(/é/g, 'e')
                            .replace(/í/g, 'i')
                            .replace(/ó/g, 'o')
                            .replace(/ú/g, 'u');
            }    
            var returnData = [];
            if(_.isArray($localStorage.searchResultList)){
                _.forEach($localStorage.searchResultList,function(value,key){
                    data = data.toLowerCase();
                    var first_name = removeAccents(value.first_name);
                    var last_name = removeAccents(value.last_name);
                    if(first_name.toLowerCase().indexOf(data.toLowerCase())>=0 ||last_name.toLowerCase().indexOf(data)>=0 || value.number_id.indexOf(data)>=0){
                        returnData.push(value);
                    }  
                })
            }
            return returnData;
        },

        profileData (data){

            if(_.isArray($localStorage.patienteData)){
            var flag = 0;
            var setKey = 0;
                _.forEach($localStorage.patienteData,function(value,key){
                    if(value.id == data.id){
                       flag++
                       setKey = key;
                    }
                })
                if(flag === 0 ){
                    $localStorage.patienteData.push(data);
                }
                else{
                    $localStorage.patienteData[setKey] = data;
                }
            }
            else{
                $localStorage.patienteData = [];
                $localStorage.patienteData.push(data);
            }
        },

        editProfileData (data){

            if(_.isArray($localStorage.editPatienteData)){
            var flag = 0;
            var setKey = 0;
                _.forEach($localStorage.editPatienteData,function(value,key){
                    if(value.id == data.id){
                       flag++
                       setKey = key;
                    }
                })
                if(flag === 0 ){
                    $localStorage.editPatienteData.push(data);
                }
                else{
                    $localStorage.editPatienteData[setKey] = data;
                }
            }
            else{
                $localStorage.editPatienteData = [];
                $localStorage.editPatienteData.push(data);
            }
        },
        getProfileData (id){
            var returnData = {};
            if(_.isArray($localStorage.patienteData)){
                returnData =  _.find($localStorage.patienteData,function(data){

                    return parseInt(data.id) === parseInt(id);
                });
                if(!returnData){
                    returnData = [];
                }
            }
            return returnData;
        },

        getEditProfileData (id){
            var returnData = {};
            if(_.isArray($localStorage.editPatienteData)){
                returnData =  _.find($localStorage.editPatienteData,function(data){

                    return parseInt(data.id) === parseInt(id);
                });
                if(!returnData){
                    returnData = [];
                }
            }
            return returnData;
        },

        storeMedicalData (data,response){
            if(!_.isEmpty($localStorage.medicalData)){

                if (data.toLowerCase().indexOf("hombro") >= 0) {

                    if(!_.isEmpty($localStorage.medicalData.hombro)){
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            $localStorage.medicalData.hombro.consulta = response;
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            $localStorage.medicalData.hombro.cirugia = response;
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            $localStorage.medicalData.hombro.control = response;
                        }    
                    }
                    else{
                        $localStorage.medicalData.hombro = {};
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            $localStorage.medicalData.hombro.consulta = response;
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            $localStorage.medicalData.hombro.cirugia = response;
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            $localStorage.medicalData.hombro.control = response;
                        }    

                    }
                    
                }

                if (data.toLowerCase().indexOf("cadera") >= 0) {
                    if(!_.isEmpty($localStorage.medicalData.cadera)){
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            $localStorage.medicalData.cadera.consulta = response;
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            $localStorage.medicalData.cadera.cirugia = response;
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            $localStorage.medicalData.cadera.control = response;
                        }    
                    }
                    else{
                        $localStorage.medicalData.cadera = {};
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            $localStorage.medicalData.cadera.consulta = response;
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            $localStorage.medicalData.cadera.cirugia = response;
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            $localStorage.medicalData.cadera.control = response;
                        }    
                    }
                    
                }

                if (data.toLowerCase().indexOf("rodilla") >= 0) {
                    if(!_.isEmpty($localStorage.medicalData.rodilla)){
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            $localStorage.medicalData.rodilla.consulta = response;
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            $localStorage.medicalData.rodilla.cirugia = response;
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            $localStorage.medicalData.rodilla.control = response;
                        }    
                    }
                    else{
                        $localStorage.medicalData.rodilla = {};
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            $localStorage.medicalData.rodilla.consulta = response;
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            $localStorage.medicalData.rodilla.cirugia = response;
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            $localStorage.medicalData.rodilla.control = response;
                        }    
                    }
                    
                }

                if (data.toLowerCase().indexOf("columna") >= 0) {
                    if(!_.isEmpty($localStorage.medicalData.columna)){
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            $localStorage.medicalData.columna.consulta = response;
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            $localStorage.medicalData.columna.cirugia = response;
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            $localStorage.medicalData.columna.control = response;
                        }    
                    }
                    else{
                        $localStorage.medicalData.columna = {};
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            $localStorage.medicalData.columna.consulta = response;
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            $localStorage.medicalData.columna.cirugia = response;
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            $localStorage.medicalData.columna.control = response;
                        }    
                    }
                    
                }
            }
            else{
                $localStorage.medicalData = {};

                if (data.toLowerCase().indexOf("hombro") >= 0) {

                    if(!_.isEmpty($localStorage.medicalData.hombro)){
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            $localStorage.medicalData.hombro.consulta = response;
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            $localStorage.medicalData.hombro.cirugia = response;
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            $localStorage.medicalData.hombro.control = response;
                        }    
                    }
                    else{
                        $localStorage.medicalData.hombro = {};
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            $localStorage.medicalData.hombro.consulta = response;
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            $localStorage.medicalData.hombro.cirugia = response;
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            $localStorage.medicalData.hombro.control = response;
                        }    

                    }
                    
                }

                if (data.toLowerCase().indexOf("cadera") >= 0) {
                    if(!_.isEmpty($localStorage.medicalData.cadera)){
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            $localStorage.medicalData.cadera.consulta = response;
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            $localStorage.medicalData.cadera.cirugia = response;
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            $localStorage.medicalData.cadera.control = response;
                        }    
                    }
                    else{
                        $localStorage.medicalData.cadera = {};
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            $localStorage.medicalData.cadera.consulta = response;
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            $localStorage.medicalData.cadera.cirugia = response;
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            $localStorage.medicalData.cadera.control = response;
                        }    
                    }
                    
                }

                if (data.toLowerCase().indexOf("rodilla") >= 0) {
                    if(!_.isEmpty($localStorage.medicalData.rodilla)){
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            $localStorage.medicalData.rodilla.consulta = response;
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            $localStorage.medicalData.rodilla.cirugia = response;
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            $localStorage.medicalData.rodilla.control = response;
                        }    
                    }
                    else{
                        $localStorage.medicalData.rodilla = {};
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            $localStorage.medicalData.rodilla.consulta = response;
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            $localStorage.medicalData.rodilla.cirugia = response;
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            $localStorage.medicalData.rodilla.control = response;
                        }    
                    }
                    
                }

                if (data.toLowerCase().indexOf("columna") >= 0) {
                    if(!_.isEmpty($localStorage.medicalData.columna)){
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            $localStorage.medicalData.columna.consulta = response;
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            $localStorage.medicalData.columna.cirugia = response;
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            $localStorage.medicalData.columna.control = response;
                        }    
                    }
                    else{
                        $localStorage.medicalData.columna = {};
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            $localStorage.medicalData.columna.consulta = response;
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            $localStorage.medicalData.columna.cirugia = response;
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            $localStorage.medicalData.columna.control = response;
                        }    
                    }
                    
                }

            }

            console.log($localStorage.medicalData)
        },

        getMedicalData (data){
            var medicalReturData = {};
            if(!_.isEmpty($localStorage.medicalData)){

                if (data.toLowerCase().indexOf("hombro") >= 0) {

                    if(!_.isEmpty($localStorage.medicalData.hombro)){
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            if(!_.isEmpty($localStorage.medicalData.hombro.consulta)){
                                medicalReturData = $localStorage.medicalData.hombro.consulta;
                            }
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            if(!_.isEmpty($localStorage.medicalData.hombro.cirugia)){
                                medicalReturData = $localStorage.medicalData.hombro.cirugia;
                            }
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            if(!_.isEmpty($localStorage.medicalData.hombro.control)){
                                medicalReturData = $localStorage.medicalData.hombro.control;
                            }
                        }    
                    }
                    
                }

                if (data.toLowerCase().indexOf("cadera") >= 0) {
                    if(!_.isEmpty($localStorage.medicalData.cadera)){
                        if (data.toLowerCase().indexOf("consulta") >= 0) { 
                            if(!_.isEmpty($localStorage.medicalData.cadera.consulta)){
                                medicalReturData = $localStorage.medicalData.cadera.consulta;
                            }
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                            if(!_.isEmpty($localStorage.medicalData.cadera.cirugia)){
                                medicalReturData = $localStorage.medicalData.cadera.cirugia;
                            }
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            if(!_.isEmpty($localStorage.medicalData.cadera.control)){
                                medicalReturData = $localStorage.medicalData.cadera.control;
                            }
                        }    
                    }
                    
                }

                if (data.toLowerCase().indexOf("rodilla") >= 0) {
                    if(!_.isEmpty($localStorage.medicalData.rodilla)){
                        if (data.toLowerCase().indexOf("consulta") >= 0) {
                            if(!_.isEmpty($localStorage.medicalData.rodilla.consulta)){
                                medicalReturData = $localStorage.medicalData.rodilla.consulta;
                            }
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) {
                            if(!_.isEmpty($localStorage.medicalData.rodilla.cirugia)){
                                medicalReturData = $localStorage.medicalData.rodilla.cirugia;
                            }
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            if(!_.isEmpty($localStorage.medicalData.rodilla.control)){
                                medicalReturData = $localStorage.medicalData.rodilla.control;
                            }
                        }    
                    }
                }

                if (data.toLowerCase().indexOf("columna") >= 0) {
                    if(!_.isEmpty($localStorage.medicalData.columna)){
                        if (data.toLowerCase().indexOf("consulta") >= 0) {
                            if(!_.isEmpty($localStorage.medicalData.columna.consulta)){
                                medicalReturData = $localStorage.medicalData.columna.consulta;
                            }
                        }    
                        if (data.toLowerCase().indexOf("cirugia") >= 0) {
                            if(!_.isEmpty($localStorage.medicalData.columna.cirugia)){
                                medicalReturData = $localStorage.medicalData.columna.cirugia;
                            }
                        }    
                        if (data.toLowerCase().indexOf("control") >= 0) { 
                            if(!_.isEmpty($localStorage.medicalData.columna.control)){
                                medicalReturData = $localStorage.medicalData.columna.control;
                            }
                        }    
                    }
                }
            }
            return medicalReturData;
        },

        storeSubSegmentSelection(section,data,response){

            if(_.isEmpty($localStorage.medicalData[section])){
                console.log($localStorage.medicalData[section])
                if (data.toLowerCase().indexOf("consulta") >= 0) { 
                    $localStorage.medicalData[section].consulta = response;
                    console.log($localStorage.medicalData[section]);
                }

                if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                    $localStorage.medicalData[section].cirugia = response; 
                    console.log($localStorage.medicalData[section]);  
                    
                }

                if (data.toLowerCase().indexOf("control") >= 0) { 
                    $localStorage.medicalData[section].control = response;  
                    console.log($localStorage.medicalData[section]); 
                }  
            }
            else{

                if (data.toLowerCase().indexOf("consulta") >= 0) { 
                    $localStorage.medicalData[section].consulta = response;   
                }

                if (data.toLowerCase().indexOf("cirugia") >= 0) { 
                    $localStorage.medicalData[section].cirugia = response;   
                    
                }

                if (data.toLowerCase().indexOf("control") >= 0) { 
                    $localStorage.medicalData[section].control = response;   
                }  
            }
        },

        getSubSegmentSelection(section,data){
            var returnData2 = {};
            if(!_.isEmpty($localStorage.medicalData[section])){
                if (data.toLowerCase().indexOf("consulta") >= 0) { 
                    if(!_.isEmpty($localStorage.medicalData[section].consulta)){
                        returnData2 = $localStorage.medicalData[section].consulta;
                    }
                    else{
                        returnData2 = false;
                    }
                }

                if (data.toLowerCase().indexOf("cirugia") >= 0) {  
                    if(!_.isEmpty($localStorage.medicalData[section].cirugia)){
                        returnData2 = $localStorage.medicalData[section].cirugia;
                    }
                    else{
                        returnData2 = false;
                    }
                    
                }

                if (data.toLowerCase().indexOf("control") >= 0) {  
                    if(!_.isEmpty($localStorage.medicalData[section].control)){
                        returnData2 = $localStorage.medicalData[section].control;
                    }
                    else{
                        returnData2 = false;
                    }
                }  
            }
            return returnData2;
        },

        storeHistoryData(data, patient_id){
            console.log(data);
            console.log(patient_id);
            if(!_.isEmpty($localStorage.historyData)){
                // The `_.matches` iteratee shorthand.
                var index = _.findIndex($localStorage.historyData, { 'patient_id' : patient_id });
                var hData = {
                    'data':data,
                    'patient_id':patient_id
                }
                if(index>=0){
                    $localStorage.historyData[index] =  hData;
                    console.log($localStorage.historyData);   
                }
                else{
                    $localStorage.historyData.push(hData);
                    console.log($localStorage.historyData);
                }
            }
            else{
                $localStorage.historyData = [];
                var hData = {
                    'data':data,
                    'patient_id':patient_id
                }
                $localStorage.historyData.push(hData);
                console.log($localStorage.historyData);
            }
        },

        getHistoryData(patient_id){
            var returnData = [];
            if(!_.isEmpty($localStorage.historyData)){
                var index = _.findIndex($localStorage.historyData,{'patient_id':patient_id});
                if(index>=0){
                    returnData = $localStorage.historyData[index].data;
                }
                else{
                    returnData = [];
                }
            }
            return returnData;
        }      


    };
})

.factory('offlineWebservice', function($localStorage,$http,$rootScope,webService,FlashMessage) {
    return {
    storeApiRequest(data) {
            if(_.isArray($localStorage.apiData)){
                $localStorage.apiData.push(data);
            }
            else{
                $localStorage.apiData = [];
                $localStorage.apiData.push(data);
            }
            console.log("added");
        },

    updateApiRequest() {
        console.log("here")
            if(_.isEmpty($localStorage.apiData)){
            }
            else{
                for (var i = 0; i <= $localStorage.apiData.length-1; i++) {
                        if($localStorage.apiData[i].type == 'form'){
                            $rootScope.showLoadingText('synching datas');
                            $.post($localStorage.apiData[i].api, JSON.stringify($localStorage.apiData[i].data),function(response){
                                console.log(response);
                                $rootScope.hideLoading();
                            }).error(function(){
                                console.log("error")
                                $rootScope.hideLoading();

                            });
                        }
                        else{
                            if($localStorage.apiData[i].type == 'patient_creation'){ 
                                $rootScope.showLoadingText('synching datas');
                                webService.invokeService($localStorage.apiData[i].api,$localStorage.apiData[i].data)
                                .success(function(response){
                                    $rootScope.hideLoading();
                                    if (response.success) {
                                        FlashMessage.showInformation('El paciente ha sido creado');
                                    }
                                })
                                .error(function(errorCode){
                                    $rootScope.hideLoading();
                                    console.log(errorCode);
                                    if (errorCode == 401) {
                                        Auth.logout();
                                    } else {
                                        if(errorCode == 422){
                                            FlashMessage.showInformation('Some error occured in your patient create form, Please try again now');
                                        }
                                        else{
                                            FlashMessage.showError('something went wrong');
                                        }
                                    }
                                })
                            }
                            if($localStorage.apiData[i].type == 'update_profile'){ 
                                $rootScope.showLoadingText('synching datas');
                                webService.invokeService($localStorage.apiData[i].api,$localStorage.apiData[i].data)
                                .success(function(response){
                                    $rootScope.hideLoading();
                                     if (response.success) {
                                        FlashMessage.showInformation('El paciente ha sido creado');
                                    }

                                })
                                .error(function(errorCode){
                                    $rootScope.hideLoading();
                                    console.log(errorCode);
                                    if (errorCode == 401) {
                                        Auth.logout();
                                    } else {
                                        if(errorCode == 422){
                                            FlashMessage.showError('Some error occured in your patient create form, Please try again now');
                                        }
                                        else{
                                            FlashMessage.showError('Some error occured in your patient create form, Please try again now');
                                        }
                                    }
                                })
                            }
                            if($localStorage.apiData[i].type == 'patient_edit'){ 
                                $rootScope.showLoadingText('synching datas');
                                webService.invokeService($localStorage.apiData[i].api,$localStorage.apiData[i].data)
                                .success(function(response){
                                    $rootScope.hideLoading();
                                     if (response.success) {
                                        FlashMessage.showInformation('El paciente ha sido modificado');
                                    }

                                })
                                .error(function(errorCode){
                                    $rootScope.hideLoading();
                                    console.log(errorCode);
                                    if (errorCode == 401) {
                                        Auth.logout();
                                    } else {
                                        if(errorCode == 422){
                                            FlashMessage.showError('Some error occured in your patient create form, Please try again now');
                                        }
                                        else{
                                            FlashMessage.showError('Some error occured in your patient create form, Please try again now');
                                        }
                                    }
                                })
                            }
                        }

                        // $http.post($localStorage.apiData[i].api, $localStorage.apiData[i].data)
                        // .success(function(){
                        //     console.log("success")
                        //     $localStorage.apiData.splice(i,1);
                        //     $rootScope.hideLoading();
                        //     return true;
                        // })
                        // .error(function(){
                        //     console.log("error")
                        //     $rootScope.hideLoading();
                        //     return false;
                        
                }
                $localStorage.apiData = [];
            }
        }
    }
})

.factory('ImageService', function($cordovaCamera) {

    function optionsForType(type) {
        var source;
        switch (type) {
            case 0:
                source = Camera.PictureSourceType.CAMERA;
                break;
            case 1:
                source = Camera.PictureSourceType.PHOTOLIBRARY;
                break;
        }
        return {
            destinationType : Camera.DestinationType.DATA_URL,
            //destinationType: Camera.DestinationType.FILE_URI,
            sourceType: source,
            allowEdit: false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 800,
            targetHeight: 800,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };
    }

    function saveMedia(type, callback) {
        var options = optionsForType(type);

        $cordovaCamera.getPicture(options).then(function(image) {
            callback(false, image);
        }, function(err) {
            callback(true, err);
        });
    }

    return {
        handleMediaDialog: saveMedia
    }
})

.factory('ConnectivityMonitor', function($rootScope, $cordovaNetwork, $cordovaToast,offlineWebservice){
 
  return {
    isOnline: function(){
      if(ionic.Platform.isWebView()){
        return $cordovaNetwork.isOnline();   
      } else {
        return navigator.onLine;
      }
    },
    isOffline: function(){
      if(ionic.Platform.isWebView()){
        return !$cordovaNetwork.isOnline();   
      } else {
        return !navigator.onLine;
      }
    },
    startWatching: function(){
        if(ionic.Platform.isWebView()){
 
          $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            console.log("Conectado");
            offlineWebservice.updateApiRequest();
            $cordovaToast.showLongBottom("Conectado");
          });
 
          $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            console.log("Desconectado");
            $cordovaToast.showLongBottom("Desconectado");
          });
 
        }
        else {
 
          window.addEventListener("online", function(e) {
          }, false);   
 
          window.addEventListener("offline", function(e) {
            console.log("went offline");
          }, false); 
        }      
    }
  }
})

.run(function($rootScope, Auth, $http){
    Auth.checkLogged();

    $rootScope.$on('$routeChangeStart', function(){
        Auth.checkLogged();
    });

    $rootScope.$on('$locationChangeStart', function(){
        Auth.checkLogged();
    });

    if (Auth.isLogged()){
        //$http.defaults.headers.common['Auth-Token'] = Auth.token();
    }
});