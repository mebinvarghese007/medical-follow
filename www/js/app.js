angular.module('app', ['ionic', 'ion-datetime-picker','ngLodash','ngStorage' ,'ionic-datepicker', 'ionic-timepicker', 'ionic-modal-select', 'app.controllers', 'app.routes', 'app.services', 'app.directives', 'ngSanitize', 'ngCordova', 'rwdImageMaps', 'ngTagsInput'])

.constant('endPoint', 'https://medicalfollow.com/api/v1/')
// .constant('endPoint', 'http://datapromed.com/promeddata_dev/api/v1/')
 //.constant('endPoint', 'http://datapromed.com/api/v1/')
// .constant('endPoint', 'http://localhost:8000/api/v1/')

        .run(function ($ionicPlatform, $rootScope, $ionicLoading, endPoint, $ionicPickerI18n, $cordovaCamera,ConnectivityMonitor) {
            $ionicPlatform.ready(function () {
                // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                // for form inputs)
                if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                    cordova.plugins.Keyboard.disableScroll(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                    // org.apache.cordova.statusbar required
                    //StatusBar.overlaysWebView(true);

                    //StatusBar.backgroundColorByHexString('#3e6fc1');
                    // hide the status bar using the StatusBar plugin
                    //StatusBar.hide();
                }
            });
            $rootScope.showLoading = function () {
                $ionicLoading.show({
                    template: "<ion-spinner></ion-spinner>"
                });
            };

            $rootScope.showLoadingText = function (text) {
                $ionicLoading.show({
                    template: text
                });
            };

            $rootScope.hideLoading = function () {
                $ionicLoading.hide();
            };

            $rootScope.endPoint = endPoint;

            $ionicPickerI18n.cancel = "Cancelar";

            ConnectivityMonitor.startWatching();
        })
        .config(function (ionicDatePickerProvider) {
            var datePickerObj = {
                inputDate: new Date(),
                setLabel: 'Seleccionar',
                todayLabel: 'Hoy',
                closeLabel: 'Cerrar',
                mondayFirst: true,
                weeksList: ["D", "L", "M", "Mi", "J", "V", "S"],
                monthsList: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"],
                templateType: 'popup',
                from: new Date(2012, 8, 1),
                to: new Date(2016, 8, 1),
                showTodayButton: false,
                dateFormat: 'dd MMMM yyyy',
                closeOnSelect: true,
                disableWeekdays: []
            };
            ionicDatePickerProvider.configDatePicker(datePickerObj);
        });
